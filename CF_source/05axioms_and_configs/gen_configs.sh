#!/bin/sh

mkdir -p configs

for class in `grep '^[^#]' classes.txt`
do
	echo $class
	file=`echo "$class" |sed 's/[^a-zA-Z0-9_-]/_/g'`
	echo $file
	conffile="configs/$file.properties"
	perl -pne "s|\[class\]|$class|g; s|\[file\]|$file|g" <template.properties >"$conffile"
#	cat template.properties |
#		sed "s/\[class\]/$class/g" |
#		sed "s/\[file\]/$file/g"
#		>$conffile
done
