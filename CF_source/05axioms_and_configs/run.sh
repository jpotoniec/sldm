#!/bin/sh

MEM=128G
mkdir -p logs/
mkdir -p results/

for conf in configs/*.properties
#for conf in configs/http___dbpedia_org_ontology_Crater.properties
do
	basename=`basename "$conf" .properties`
	logfile="logs/$basename.log"
	if [ ! -f "results/$basename.rdf" ]
	then
		echo $conf
		java -Xmx$MEM -ea:put.sldm... -cp ../SLDMDesktop/target/SLDMDesktop-1.0-SNAPSHOT.jar 'put.sldm.desktop.BatchRunner' $conf 2>&1 > $logfile
	fi
done
