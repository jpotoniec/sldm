#instructions
File instructions.txt contains the content, that is visible for contributors on CrowdFlower platform as tips while they are doing CF jobs.

#Source
The file [source05.csv](source05.csv) and [source08.csv](source08.csv) contain source data for CrowdFlower for the experiments with minimal support threshold 0.5 and 0.8 respectively.
Format of these files is described on CF documentation website https://success.crowdflower.com/hc/en-us/articles/202703175-How-to-Format-Data-for-Uploading

In the directories [05axioms_and_configs/](05axioms_and_configs/) and [08axioms_and_configs/](08axioms_and_configs/) there are orignal configuration files for SLDM, the log files from mining and the obtained axioms (resp. for the experiment with minimal support threshold 0.5 and 0.8)

List of axioms that were removed before the experiment is available in [removed_axioms.txt](removed_axioms.txt).

#Results
In the results directories are reports from CF experiment
