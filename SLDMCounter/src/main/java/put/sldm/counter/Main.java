package put.sldm.counter;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.datatypes.DatatypeRegistry;
import org.semanticweb.HermiT.datatypes.doublenum.DoubleDatatypeHandler;
import org.semanticweb.HermiT.datatypes.owlreal.UsDollarDatatypeHandler;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) throws OWLOntologyCreationException {
        String baselineOntologyFile = args[0];
        String subclassAxiomsFile = args[1];
        String superclassAxiomsFile = args.length > 2 ? args[2] : null;
        new GYearDatatypeHandler(); //side effect from the static constructor
        DatatypeRegistry.registerDatatypeHandler(new UsDollarDatatypeHandler());
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology baselineOntology = manager.loadOntologyFromOntologyDocument(new File(baselineOntologyFile));
        OWLReasoner baselineReasoner = new Reasoner.ReasonerFactory().createReasoner(baselineOntology);
        OWLOntology subclassAxioms = manager.loadOntologyFromOntologyDocument(new File(subclassAxiomsFile));
        OWLOntology superclassWithBaseline = manager.createOntology();
        manager.addAxioms(superclassWithBaseline, baselineOntology.getAxioms());
        if (superclassAxiomsFile != null) {
            OWLOntology superclassAxioms = manager.loadOntologyFromOntologyDocument(new File(superclassAxiomsFile));
            manager.addAxioms(superclassWithBaseline, superclassAxioms.getAxioms());
        }
        OWLReasoner superclassReasoner = new Reasoner.ReasonerFactory().createReasoner(superclassWithBaseline);
        int axiomCounter = 0, baselineCounter = 0, superclassCounter = 0;
        for (OWLAxiom axiom : subclassAxioms.getAxioms(AxiomType.SUBCLASS_OF)) {
//            System.out.println(axiom);
            axiomCounter++;
            try {
                boolean baseline = baselineReasoner.isEntailed(axiom);
//                System.out.println("\t" + baseline);
                if (baseline)
                    baselineCounter++;
            } catch (Exception e) {
            }
            try {
                boolean superclass = superclassReasoner.isEntailed(axiom);
//                System.out.println("\t" + superclass);
                if (superclass)
                    superclassCounter++;
            } catch (Exception e) {
            }
        }
        System.out.println(String.format("%d %d %d", axiomCounter, baselineCounter, superclassCounter));
    }
}
