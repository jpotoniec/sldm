package put.sldm.counter;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.datatypes.DatatypeRegistry;
import org.semanticweb.HermiT.datatypes.owlreal.UsDollarDatatypeHandler;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Group {

    public static void main(String[] args) throws OWLOntologyCreationException {
        String ontologyFile = args[0];
        new GYearDatatypeHandler(); //side effect from the static constructor
        DatatypeRegistry.registerDatatypeHandler(new UsDollarDatatypeHandler());
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology baselineOntology = manager.loadOntologyFromOntologyDocument(new File(ontologyFile));
        List<OWLOntology> ontologies = new ArrayList<>();
        Set<OWLSubClassOfAxiom> axioms = new HashSet<>();
        int n = 0;
        for (int i = 1; i < args.length; ++i) {
            OWLOntology a = manager.loadOntologyFromOntologyDocument(new File(args[i]));
            ontologies.add(a);
            Set<OWLSubClassOfAxiom> tmp = a.getAxioms(AxiomType.SUBCLASS_OF);
            axioms.addAll(tmp);
            n += tmp.size();
        }
//        System.out.println(String.format("%d distinct axioms in the set of %d axioms", axioms.size(), n));
        int[] hist = new int[ontologies.size()+1];
        List<OWLReasoner> reasoners=new ArrayList<>();
        for (int i = 0; i < ontologies.size(); ++i) {
            OWLOntology tmp = manager.createOntology();
            manager.addAxioms(tmp, ontologies.get(i).getAxioms());
            manager.addAxioms(tmp, baselineOntology.getAxioms());
            OWLReasoner r = new Reasoner.ReasonerFactory().createReasoner(tmp);
            reasoners.add(r);
        }
        for (OWLAxiom axiom : axioms) {
            int m = 0;
            for (int i = 0; i < ontologies.size(); ++i) {
                if (ontologies.get(i).containsAxiom(axiom) || reasoners.get(i).isEntailed(axiom))
                    m++;
            }
            hist[m]++;
        }
        System.out.print(String.format("%s & %d & %d ", axioms.iterator().next().getSubClass(), axioms.size(), n));
        for(int i=0;i<hist.length;++i)
            System.out.print(String.format(" & %d", hist[i]));
        System.out.println("\\\\");
    }
}
