package put.sldm.counter;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.datatypes.DatatypeRegistry;
import org.semanticweb.HermiT.datatypes.owlreal.UsDollarDatatypeHandler;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Cross {

    public static void main(String[] args) throws OWLOntologyCreationException {
        String ontologyFile = args[0];
        new GYearDatatypeHandler(); //side effect from the static constructor
        DatatypeRegistry.registerDatatypeHandler(new UsDollarDatatypeHandler());
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology baselineOntology = manager.loadOntologyFromOntologyDocument(new File(ontologyFile));
        OWLReasoner baselineReasoner = new Reasoner.ReasonerFactory().createReasoner(baselineOntology);
        List<OWLOntology> axioms = new ArrayList<>();
        List<String> sourceFiles=new ArrayList<>();
        for (int i = 1; i < args.length; ++i) {
            OWLOntology a = manager.loadOntologyFromOntologyDocument(new File(args[i]));
            axioms.add(a);
            sourceFiles.add(args[i]);
        }
        for (int i = 0; i < axioms.size(); ++i) {
            OWLOntology tmp = manager.createOntology();
            manager.addAxioms(tmp, axioms.get(i).getAxioms());
            manager.addAxioms(tmp, baselineOntology.getAxioms());
            OWLReasoner r = new Reasoner.ReasonerFactory().createReasoner(tmp);
            for (int j = 0; j < axioms.size(); ++j) {
                if (i != j) {
                    int axiomsCounter = 0, entailedCounter = 0;
                    for (OWLAxiom axiom : axioms.get(j).getAxioms(AxiomType.SUBCLASS_OF)) {
                        if(!baselineReasoner.isEntailed(axiom)) {
                            axiomsCounter++;
                            if (r.isEntailed(axiom))
                                entailedCounter++;
                        }
                    }
                    System.out.println(String.format("%s in %s %d/%d", sourceFiles.get(j), sourceFiles.get(i), entailedCounter, axiomsCounter));
                }
            }
        }
    }
}
