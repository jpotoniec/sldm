package put.sldm.counter;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.datatypes.DatatypeRegistry;
import org.semanticweb.HermiT.datatypes.owlreal.UsDollarDatatypeHandler;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CountEntailments {

    public static void main(String[] args) throws OWLOntologyCreationException {
        String ontologyFile = args[0];
        new GYearDatatypeHandler(); //side effect from the static constructor
        DatatypeRegistry.registerDatatypeHandler(new UsDollarDatatypeHandler());
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology baselineOntology = manager.loadOntologyFromOntologyDocument(new File(ontologyFile));
        OWLReasoner baselineReasoner = new Reasoner.ReasonerFactory().createReasoner(baselineOntology);
        for (int i = 1; i < args.length; ++i) {
            OWLOntology a = manager.loadOntologyFromOntologyDocument(new File(args[i]));
            int entailed=0;
            Set<OWLSubClassOfAxiom> axioms = a.getAxioms(AxiomType.SUBCLASS_OF);
            for (OWLAxiom axiom : axioms) {
                try {
                    if(baselineReasoner.isEntailed(axiom))
                        entailed++;
                } catch(Exception e) {
                }
            }
            int n=axioms.size();
            System.out.println(String.format("%s %d %d", args[i], n, entailed));
        }
    }
}
