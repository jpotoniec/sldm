package put.sldm.counter;

import org.semanticweb.HermiT.datatypes.DatatypeHandler;
import org.semanticweb.HermiT.datatypes.MalformedLiteralException;
import org.semanticweb.HermiT.datatypes.UnsupportedFacetException;
import org.semanticweb.HermiT.datatypes.ValueSpaceSubset;
import org.semanticweb.HermiT.datatypes.owlreal.*;
import org.semanticweb.HermiT.model.DatatypeRestriction;

import java.util.Arrays;
import java.util.Set;

public class GYearDatatypeHandler extends OWLRealDatatypeHandler {
    static {
        {
            String datatypeURI = XSD_NS + "gYear";
            NumberInterval interval = new NumberInterval(NumberRange.INTEGER, NumberRange.NOTHING, MinusInfinity.INSTANCE, BoundType.EXCLUSIVE, PlusInfinity.INSTANCE, BoundType.EXCLUSIVE);
            s_intervalsByDatatype.put(datatypeURI, interval);
            s_subsetsByDatatype.put(datatypeURI, new OWLRealValueSpaceSubset(interval));
        }
    }

    @Override
    public Object parseLiteral(String lexicalForm, String datatypeURI) throws MalformedLiteralException {
        try {
            if ("http://dbpedia.org/datatype/usDollar".equals(datatypeURI))
                return Numbers.parseDecimal(lexicalForm);
        } catch (Exception e) {
            ;
        }
        return super.parseLiteral(lexicalForm, datatypeURI);
    }
}
