package put.leolod.sldmprotege;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import org.protege.editor.owl.OWLEditorKit;
import org.protege.editor.owl.model.util.OWLAxiomInstance;
import org.protege.editor.owl.ui.axiom.AxiomAnnotationsList;
import org.protege.editor.owl.ui.renderer.OWLCellRenderer;

/**
 * Copied from org.protege.editor.owl.ui.axiom.AxiomAnnotationPanel
 */
public class AxiomAnnotationReadOnlyPanel extends JComponent {

    private AxiomAnnotationsList annotationsComponent;

    private DefaultListModel model;

    public AxiomAnnotationReadOnlyPanel(OWLEditorKit eKit) {
        setLayout(new BorderLayout(6, 6));
        setPreferredSize(new Dimension(500, 300));

        // we need to use the OWLCellRenderer, so create a singleton JList
        final OWLCellRenderer ren = new OWLCellRenderer(eKit);
        ren.setHighlightKeywords(true);

        model = new DefaultListModel();
        JList label = new JList(model);
        label.setBackground(getBackground());
        label.setEnabled(false);
        label.setOpaque(true);
        label.setCellRenderer(ren);

        annotationsComponent = new AxiomAnnotationsReadOnlyList(eKit);

        final JScrollPane scroller = new JScrollPane(annotationsComponent);

        add(label, BorderLayout.NORTH);
        add(scroller, BorderLayout.CENTER);

        setVisible(true);
    }

    public void setAxiomInstance(OWLAxiomInstance axiomInstance) {
        model.clear();
        if (axiomInstance != null) {
            model.addElement(axiomInstance.getAxiom());
            annotationsComponent.setRootObject(axiomInstance);
        } else {
            annotationsComponent.setRootObject(null);
        }
    }

    public OWLAxiomInstance getAxiom() {
        return annotationsComponent.getRoot();
    }

    public void dispose() {
        annotationsComponent.dispose();
    }

}
