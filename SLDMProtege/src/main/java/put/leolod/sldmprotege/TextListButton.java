package put.leolod.sldmprotege;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import org.protege.editor.core.ui.list.MListButton;
import org.protege.editor.owl.ui.renderer.OWLRendererPreferences;

public class TextListButton extends MListButton {

    private String label;
    public static final Color ROLL_OVER_COLOR = new Color(0, 0, 0);

    public TextListButton(String label, String name, ActionListener actionListener) {
        super(name, ROLL_OVER_COLOR, actionListener);
        this.label = label;
    }

    /**
     * Copied from org.protege.editor.owl.ui.framelist.AxiomAnnotationButton     
     */
    @Override
    public void paintButtonContent(Graphics2D g) {
        int w = getBounds().width;
        int h = getBounds().height;
        int x = getBounds().x;
        int y = getBounds().y;

        Font font = g.getFont().deriveFont(Font.BOLD, OWLRendererPreferences.getInstance().getFontSize());
        g.setFont(font);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        final Rectangle stringBounds = fontMetrics.getStringBounds(label, g).getBounds();
        int baseline = fontMetrics.getLeading() + fontMetrics.getAscent();
        g.drawString(label, x + w / 2 - stringBounds.width / 2, y + (h - stringBounds.height) / 2 + baseline);
    }
}
