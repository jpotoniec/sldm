package put.leolod.sldmprotege;

import put.sldm.patterns.FullPattern;

public interface Processor {

    public void process(FullPattern pattern);

    public void done();

    public void stateChanged(String state);
}
