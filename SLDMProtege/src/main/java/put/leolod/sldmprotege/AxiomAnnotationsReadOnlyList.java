package put.leolod.sldmprotege;

import java.util.Collections;
import java.util.List;
import org.protege.editor.core.ui.list.MListButton;
import org.protege.editor.owl.OWLEditorKit;
import org.protege.editor.owl.ui.axiom.AxiomAnnotationsList;

public class AxiomAnnotationsReadOnlyList extends AxiomAnnotationsList {

    public AxiomAnnotationsReadOnlyList(OWLEditorKit eKit) {
        super(eKit);
    }

    @Override
    protected List<MListButton> getButtons(Object value) {
        return Collections.EMPTY_LIST;
    }

    @Override
    protected void handleAdd() {
        //ignore
    }

    @Override
    protected void handleDelete() {
        //ignore
    }

    @Override
    protected void handleEdit() {
        //ignore
    }

}
