package put.leolod.sldmprotege;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;
import javax.swing.*;

import org.apache.log4j.Logger;
import org.protege.editor.core.ui.view.DisposableAction;
import org.protege.editor.owl.model.selection.OWLSelectionModelListener;
import org.protege.editor.owl.ui.frame.AxiomListFrame;
import org.protege.editor.owl.ui.framelist.OWLFrameList;
import org.protege.editor.owl.ui.view.AbstractOWLViewComponent;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import put.sldm.AnnotationVocabulary;
import put.sldm.patterns.DefaultVariableGenerator;
import put.sldm.patterns.FullPattern;
import put.sldm.sampling.SamplingStrategyFactory;


/*
 * To enable debugging, in log4j.xml in Protege directory change the following:
 * 1. Treshold parameter of console appender (line 7) to trace
 * 2. Add following XML snippet below appenders:
 <category name="put.leolod">
 <priority value="all"/>
 </category>
 */
public class SLDMRunner extends AbstractOWLViewComponent {

    private static final Logger logger = Logger.getLogger(SLDMRunner.class.getName());

    private JPanel basic;
    private JPanel expert;
    private JTextField endpoint;
    private JLabel progress;
    private AxiomListFrame frame;
    private OWLFrameList<Set<OWLAxiom>> frameList;
    private SLDMManager manager;

    private void createTabs() {
        setLayout(new GridBagLayout());
        JTabbedPane tabs = new JTabbedPane();
        tabs.add("Basic", basic);
        tabs.add("Expert", expert);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 1;
        c.gridheight = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        setLayout(new GridBagLayout());
        add(tabs, c);
    }

    private void createBasic() {
        basic = new JPanel();
        GroupLayout layout = new GroupLayout(basic);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        basic.setLayout(layout);
        JLabel endpointLabel = new JLabel("SPARQL endpoint");
        endpoint = new JTextField();
        progress = new JLabel();
        frame = new AxiomListFrame(getOWLEditorKit());
        frameList = new MinedAxiomsList(getOWLEditorKit(), frame);
        frameList.setRootObject(Collections.EMPTY_SET);
        JScrollPane frameScroll = new JScrollPane(frameList);

        layout.setHorizontalGroup(layout.createParallelGroup()
                .addGroup(layout.createSequentialGroup()
                        .addComponent(endpointLabel)
                        .addComponent(endpoint)
                )
                .addComponent(progress)
                .addComponent(frameScroll)
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(endpointLabel).addComponent(endpoint))
                .addComponent(progress)
                .addComponent(frameScroll)
        );
    }

    private JLabel minSupportLabel, ignoredPropertiesLabel, maximalLevelLabel, sampleSizeLabel, randomSeedLabel, maxValuesSizeLabel, samplingStrategyLabel;
    private JSpinner minSupport, maxLevel, sampleSize, randomSeed, maxValuesSize;
    private JTextArea ignoredProperties;
    private JCheckBox useSampling;
    private JComboBox<SamplingStrategyFactory.SamplingStrategyId> samplingStrategy;

    private void createExpert() {
        expert = new JPanel();
        GroupLayout layout = new GroupLayout(expert);
        expert.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        minSupportLabel = new JLabel("Minimal support");
        minSupport = new JSpinner(new SpinnerNumberModel(0.9, 0, 1, 0.1));
        ignoredPropertiesLabel = new JLabel("<html>Ignored properties:<br>Regular expression patterns, one per line</html>");
        maximalLevelLabel = new JLabel("Maximum level");
        maxLevel = new JSpinner(new SpinnerNumberModel(2, 0, 100, 1));
        ignoredProperties = new JTextArea();
        sampleSizeLabel = new JLabel("Sample size");
        sampleSize = new JSpinner(new SpinnerNumberModel(1000, 1, null, 10));
        randomSeedLabel = new JLabel("Random seed");
        randomSeed = new JSpinner(new SpinnerNumberModel(142411297l, null, null, 1));
        samplingStrategyLabel = new JLabel("Sampling strategy");
        samplingStrategy = new JComboBox<>(SamplingStrategyFactory.SamplingStrategyId.values());
        useSampling = new JCheckBox(new AbstractAction("Use sampling") {

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean enabled = useSampling.isSelected();
                sampleSizeLabel.setEnabled(enabled);
                sampleSize.setEnabled(enabled);
                randomSeedLabel.setEnabled(enabled);
                randomSeed.setEnabled(enabled);
                samplingStrategy.setEnabled(enabled);
            }
        });
        maxValuesSizeLabel = new JLabel("Max VALUES size");
        maxValuesSize = new JSpinner(new SpinnerNumberModel(100, 1, null, 10));
        useSampling.setSelected(true);
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup()
                        .addComponent(minSupportLabel)
                        .addComponent(maximalLevelLabel)
                        .addComponent(ignoredPropertiesLabel)
                        .addComponent(useSampling)
                        .addComponent(sampleSizeLabel)
                        .addComponent(randomSeedLabel)
                        .addComponent(samplingStrategyLabel)
                        .addComponent(maxValuesSizeLabel)
                )
                .addGroup(layout.createParallelGroup()
                        .addComponent(minSupport)
                        .addComponent(maxLevel)
                        .addComponent(ignoredProperties)
                        .addComponent(sampleSize)
                        .addComponent(randomSeed)
                        .addComponent(samplingStrategy)
                        .addComponent(maxValuesSize)
                )
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(minSupportLabel).addComponent(minSupport))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(maximalLevelLabel).addComponent(maxLevel))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(ignoredPropertiesLabel).addComponent(ignoredProperties))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(useSampling))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(sampleSizeLabel).addComponent(sampleSize))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(randomSeedLabel).addComponent(randomSeed))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(samplingStrategyLabel).addComponent(samplingStrategy))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(maxValuesSizeLabel).addComponent(maxValuesSize))
        );
    }

    private Processor createProcessor(final OWLClass subclass) {
        return new Processor() {

            private Set<OWLAxiom> axioms = new HashSet<>();

            @Override
            public void process(FullPattern pattern) {
                OWLDataFactory f = getOWLDataFactory();
                OWLAnnotationProperty supportProperty = f.getOWLAnnotationProperty(AnnotationVocabulary.QUALITY);
                OWLAnnotationProperty sparqlProperty = f.getOWLAnnotationProperty(AnnotationVocabulary.SPARQL_REPRESENTATION);
                OWLClassExpression clazz = pattern.createClass(f);
                Set<OWLAnnotation> annotations = new HashSet<>();
                annotations.add(f.getOWLAnnotation(supportProperty, f.getOWLLiteral(pattern.estimateQuality())));
                annotations.add(f.getOWLAnnotation(sparqlProperty, f.getOWLLiteral(pattern.toSPARQL("?x", new DefaultVariableGenerator()))));
                axioms.add(f.getOWLSubClassOfAxiom(subclass, clazz, annotations));
                frame.setRootObject(axioms);
            }

            @Override
            public void done() {
                runAction.setIsRunning(false);
                stopAction.setEnabled(false);
            }

            @Override
            public void stateChanged(String text) {
                progress.setText("Status: " + text);
            }
        };
    }

    private class RunAction extends DisposableAction {

        private boolean isRunning = false;
        private boolean isClassSelected = false;

        public RunAction() {
            super("Run", null);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            SLDMConfig config = produceConfig();
            Processor processor = createProcessor(config.getOWLClass());
            setIsRunning(true);
            stopAction.setEnabled(true);
            manager.run(config, processor);
        }

        @Override
        public void dispose() {
        }

        public void setIsRunning(boolean isRunning) {
            this.isRunning = isRunning;
            updateStatus();
        }

        public void setIsClassSelected(boolean isClassSelected) {
            this.isClassSelected = isClassSelected;
            updateStatus();
        }

        private void updateStatus() {
            setEnabled(!isRunning && isClassSelected);
        }
    }

    private final RunAction runAction = new RunAction();
    private final DisposableAction stopAction = new DisposableAction("Stop", null) {

        @Override
        public void dispose() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            manager.stop();
        }
    };
    private final DisposableAction resetSettingsAction = new DisposableAction("Reset settings", null) {

        @Override
        public void dispose() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            loadDefaults();
        }
    };

    private OWLSelectionModelListener selectionModelListener = new OWLSelectionModelListener() {

        @Override
        public void selectionChanged() throws Exception {
            runAction.setIsClassSelected(getOWLWorkspace().getOWLSelectionModel().getLastSelectedClass() != null);
        }
    };

    @Override
    protected void initialiseOWLView() throws Exception {
        logger.trace("initialise() begin");
        addAction(runAction, "Learning", "Run SLDM");
        addAction(stopAction, "Learning", "Stop SLDM");
        addAction(resetSettingsAction, "Learning", "Stop SLDM");
        stopAction.setEnabled(false);
        createBasic();
        createExpert();
        createTabs();
        loadSettings();
        manager = new SLDMManager();
        logger.trace("initialise() end");
        getOWLWorkspace().getOWLSelectionModel().addListener(selectionModelListener);
        selectionModelListener.selectionChanged();
        this.setVisible(true);
    }

    @Override
    protected void disposeOWLView() {
        logger.trace("disposeOWLView() begin");
        getOWLWorkspace().getOWLSelectionModel().removeListener(selectionModelListener);
        saveSettings();
        manager.dispose();
        frame.dispose();
        frameList.dispose();
        logger.trace("disposeOWLView() end");
    }

    private SLDMConfig produceConfig() {
        OWLClass cls = getOWLWorkspace().getOWLSelectionModel().getLastSelectedClass();
        SLDMConfig config = new SLDMConfig();
        config.setEndpoint(endpoint.getText());
        config.setOWLClass(cls);
        config.setIgnoredProperties(ignoredProperties.getText());
        config.setMaxLevel((Integer) maxLevel.getValue());
        config.setMaxOpenLevel((Integer) maxLevel.getValue());
        config.setMaxValuesSize((Integer) maxValuesSize.getValue());
        config.setMinFunctionalSupport((Double) minSupport.getValue());
        config.setMinSupport((Double) minSupport.getValue());
        config.setRandomSeed((Long) randomSeed.getValue());
        config.setSampleSize((Integer) sampleSize.getValue());
        config.setSamplingEnabled(useSampling.isSelected());
        config.setSamplingStrategy((SamplingStrategyFactory.SamplingStrategyId) samplingStrategy.getSelectedItem());
        return config;
    }

    protected static final String PREFERENCES_KEY = SLDMRunner.class.getName();

    protected byte[] serializeSettings() {
        try (ByteArrayOutputStream data = new ByteArrayOutputStream()) {
            try (ObjectOutputStream s = new ObjectOutputStream(data)) {
                s.writeUTF(endpoint.getText());
                s.writeUTF(ignoredProperties.getText());
                s.writeInt((Integer) maxLevel.getValue());
                s.writeInt((Integer) maxLevel.getValue());
                s.writeInt((Integer) maxValuesSize.getValue());
                s.writeDouble((Double) minSupport.getValue());
                s.writeDouble((Double) minSupport.getValue());
                s.writeLong((Long) randomSeed.getValue());
                s.writeInt((Integer) sampleSize.getValue());
                s.writeBoolean(useSampling.isSelected());
                s.writeInt(samplingStrategy.getSelectedIndex());
            }
            return data.toByteArray();
        } catch (IOException ex) {
            logger.error(ex);
            return null;
        }
    }

    protected void deserializeSettings(byte[] data) {
        try (ObjectInputStream s = new ObjectInputStream(new ByteArrayInputStream(data))) {
            endpoint.setText(s.readUTF());
            ignoredProperties.setText(s.readUTF());
            maxLevel.setValue(s.readInt());
            s.readInt(); //maxOpenLevel.setValue(s.readInt());
            maxValuesSize.setValue(s.readInt());
            s.readDouble(); //minFillerSupport.setValue(s.readDouble());
            minSupport.setValue(s.readDouble());
            randomSeed.setValue(s.readLong());
            sampleSize.setValue(s.readInt());
            useSampling.setSelected(s.readBoolean());
            samplingStrategy.setSelectedIndex(s.readInt());
        } catch (IOException ex) {
            logger.error(ex);
            loadDefaults();
        }
    }

    protected void loadDefaults() {
        endpoint.setText("http://dbpedia.org/sparql");
        ignoredProperties.setText("");
        maxLevel.setValue(2);
        maxValuesSize.setValue(100);
        minSupport.setValue(.9);
        randomSeed.setValue(142411297l);
        sampleSize.setValue(1000);
        useSampling.setSelected(true);
        samplingStrategy.setSelectedItem(SamplingStrategyFactory.SamplingStrategyId.UNIFORM);
    }

    //loadSettings and saveSettings similar in spirit to RecentEditorKitManager
    protected void loadSettings() {
        Preferences userRoot = Preferences.userRoot();
        byte[] prefs = userRoot.getByteArray(PREFERENCES_KEY, null);
        if (prefs != null) {
            deserializeSettings(prefs);
        } else {
            loadDefaults();
        }
    }

    protected void saveSettings() {
        try {
            Preferences userRoot = Preferences.userRoot();
            byte[] prefs = serializeSettings();
            userRoot.putByteArray(PREFERENCES_KEY, prefs);
            userRoot.flush();
            logger.debug("Preferences saved");
        } catch (BackingStoreException ex) {
            logger.error(ex);
        }
    }
}
