package put.leolod.sldmprotege;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import org.apache.log4j.Logger;
import org.protege.editor.core.Disposable;
import put.sldm.SLDM;
import put.sldm.SLDMListener;
import put.sldm.SLDMListener.State;
import put.sldm.SPARQL;
import put.sldm.config.Config;
import put.sldm.patterns.FullPattern;

public class SLDMManager implements Disposable {

    private static final Logger logger = Logger.getLogger(SLDMRunner.class.getName());

    public SLDMManager() {
    }

    @Override
    public void dispose() {
        stop();
    }

    private class Mine extends SwingWorker<Exception, Object> {

        private final Config cfg;
        private final Processor processor;
        private final AtomicBoolean stop = new AtomicBoolean(false);

        public Mine(Config cfg, Processor processor) {
            this.cfg = cfg;
            this.processor = processor;
        }

        @Override
        protected Exception doInBackground() {
            try {
                SPARQL sparql = new SPARQL(cfg);
                SLDM sldm = new SLDM(cfg, sparql);
                sldm.run(false, new SLDMListener() {

                    @Override
                    public void miningStarted() {
                    }

                    @Override
                    public void baseDetermined(int size) {
                    }

                    @Override
                    public void miningFinished() {
                    }

                    @Override
                    public void patternMined(FullPattern p) {
                        publish(p);
                    }

                    @Override
                    public boolean shouldStop() {
                        return stop.get();
                    }

                    @Override
                    public void stateChanged(SLDMListener.State current) {
                        publish(current);
                    }
                });
                return null;
            } catch (Exception ex) {
                logger.error("SLDM defected", ex);
                return ex;
            }
        }

        private String toString(State state) {
            String text = "";
            switch (state) {
                case IDLE:
                    text = "idle";
                    break;
                case MINING:
                    text = "mining patterns";
                    break;
                case SPARQL:
                    text = "comunicating with SPARQL endpoint";
                    break;
                case VALIDATING:
                    text = "validating patterns";
                    break;
            }
            return text;
        }

        @Override
        protected void process(List<Object> chunks) {
            for (Object p : chunks) {
                if (p instanceof FullPattern) {
                    processor.process((FullPattern) p);
                } else if (p instanceof State) {
                    processor.stateChanged(toString((State) p));
                }
            }
        }

        @Override
        protected void done() {
            Throwable ex;
            try {
                ex = get();
            } catch (InterruptedException ex1) {
                ex = ex1;
            } catch (ExecutionException ex1) {
                if (ex1.getCause() != null) {
                    ex = ex1.getCause();
                } else {
                    ex = ex1;
                }
            }
            if (ex != null) {
                logger.error(ex);
                processor.stateChanged(String.format("Error: %s", ex));
            }
            processor.done();
        }

        public void stop() {
            stop.set(true);
        }

    }

    private Mine mine = null;

    public boolean isRunning() {
        return mine != null && !mine.isDone();
    }

    public void run(final Config cfg, Processor processor) {
        assert SwingUtilities.isEventDispatchThread();
        if (isRunning()) {
            throw new IllegalThreadStateException("Mining already in progress");
        }
        mine = new Mine(cfg, processor);
        mine.execute();
    }

    public void stop() {
        assert SwingUtilities.isEventDispatchThread();
        if (isRunning()) {
            mine.stop();
        }
    }
}
