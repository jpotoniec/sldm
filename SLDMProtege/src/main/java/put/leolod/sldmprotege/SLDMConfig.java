package put.leolod.sldmprotege;

import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLClass;
import put.sldm.config.Config;
import put.sldm.sampling.SamplingStrategyFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class SLDMConfig implements Config {

    private static final transient Logger logger = Logger.getLogger(SLDMConfig.class.getName());

    private String endpoint;
    private String initialQuery;
    private OWLClass cls;
    private int sampleSize;
    private boolean samplingEnabled;
    private final List<Pattern> ignoredProperties = new ArrayList<>();
    private int maxLevel;
    private int maxOpenLevel;
    private int maxValuesSize;
    private double minFunctionalSupport;
    private double minSupport;
    private long randomSeed;
    private SamplingStrategyFactory.SamplingStrategyId samplingStrategy;

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public String getInitialQuery() {
        return initialQuery;
    }

    public void setOWLClass(OWLClass cls) {
        this.cls = cls;
        initialQuery = String.format("select ?x where {?x a %s.}", cls.getIRI().toQuotedString());
    }

    public OWLClass getOWLClass() {
        return cls;
    }

    @Override
    public int getSampleSize() {
        return sampleSize;
    }

    @Override
    public boolean isSamplingEnabled() {
        return samplingEnabled;
    }

    @Override
    public SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy() {
        return samplingStrategy;
    }

    @Override
    public List<Pattern> getIgnoredProperties() {
        return Collections.unmodifiableList(ignoredProperties);
    }

    @Override
    public int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public int getMaxOpenLevel() {
        return maxOpenLevel;
    }

    @Override
    public int getMaxValuesSize() {
        return maxValuesSize;
    }

    @Override
    public double getMinFunctionalSupport() {
        return minFunctionalSupport;
    }

    @Override
    public double getMinSupport() {
        return minSupport;
    }

    @Override
    public Long getRandomSeed() {
        return randomSeed;
    }

    @Override
    public boolean getRemoteValidation() {
        return false;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    public void setSamplingEnabled(boolean samplingEnabled) {
        this.samplingEnabled = samplingEnabled;
    }

    public void setIgnoredProperties(String text) {
        String[] lines = text.split(System.lineSeparator());
        ignoredProperties.clear();
        for (String line : lines) {
            if (!line.isEmpty()) {
                ignoredProperties.add(Pattern.compile(line));
            }
        }
    }

    public void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }

    public void setMaxOpenLevel(int maxOpenLevel) {
        this.maxOpenLevel = maxOpenLevel;
    }

    public void setMaxValuesSize(int maxValuesSize) {
        this.maxValuesSize = maxValuesSize;
    }

    public void setMinFunctionalSupport(double minFunctionalSupport) {
        this.minFunctionalSupport = minFunctionalSupport;
    }

    public void setMinSupport(double minSupport) {
        this.minSupport = minSupport;
    }

    public void setRandomSeed(long randomSeed) {
        this.randomSeed = randomSeed;
    }

    public void setSamplingStrategy(SamplingStrategyFactory.SamplingStrategyId strategy) {
        this.samplingStrategy = strategy;
    }

}
