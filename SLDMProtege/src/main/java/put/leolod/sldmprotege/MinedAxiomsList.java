package put.leolod.sldmprotege;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;
import org.protege.editor.core.ui.list.MListButton;
import org.protege.editor.owl.OWLEditorKit;
import org.protege.editor.owl.model.util.OWLAxiomInstance;
import org.protege.editor.owl.ui.UIHelper;
import org.protege.editor.owl.ui.frame.OWLFrame;
import org.protege.editor.owl.ui.frame.OWLFrameSectionRow;
import org.protege.editor.owl.ui.framelist.AxiomAnnotationButton;
import org.protege.editor.owl.ui.framelist.OWLFrameList;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

public class MinedAxiomsList extends OWLFrameList<Set<OWLAxiom>> {

    private static final Logger logger = Logger.getLogger(MinedAxiomsList.class.getName());

    private final OWLEditorKit editorKit;

    public MinedAxiomsList(OWLEditorKit editorKit, OWLFrame<Set<OWLAxiom>> frame) {
        super(editorKit, frame);
        this.editorKit = editorKit;
    }

    private final static String CHECKMARK = "\u2713";

    private final TextListButton acceptAxiomButton = new TextListButton(CHECKMARK, "Accept", new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            Object value = getSelectedValue();
            if (value instanceof OWLFrameSectionRow) {
                OWLFrameSectionRow frameRow = (OWLFrameSectionRow) value;
                OWLAxiom axiom = frameRow.getAxiom();
                OWLOntology ontology = editorKit.getOWLModelManager().getActiveOntology();
                editorKit.getOWLModelManager().getOWLOntologyManager().addAxiom(ontology, axiom);
            }
        }
    });

    private final AxiomAnnotationButton axiomAnnotationButton = new AxiomAnnotationButton(new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent e) {
            invokeAxiomAnnotationHandler();
        }
    });

    private AxiomAnnotationReadOnlyPanel axiomAnnotationPanel;

    private void invokeAxiomAnnotationHandler() {
        Object obj = getSelectedValue();
        if (!(obj instanceof OWLFrameSectionRow)) {
            return;
        }
        OWLFrameSectionRow row = (OWLFrameSectionRow) obj;
        OWLAxiom ax = row.getAxiom();

        if (axiomAnnotationPanel == null) {
            axiomAnnotationPanel = new AxiomAnnotationReadOnlyPanel(editorKit);
        }
        axiomAnnotationPanel.setAxiomInstance(new OWLAxiomInstance(ax, row.getOntology()));
        new UIHelper(editorKit).showDialog("Annotations for " + ax.getAxiomType().toString(), axiomAnnotationPanel, JOptionPane.CLOSED_OPTION);
    }

    //copied from OWLFrameList
    private boolean isAnnotationPresent(OWLFrameSectionRow row) {
        OWLAxiom ax = row.getAxiom();
        return (!ax.getAnnotations().isEmpty());
    }

    @Override
    protected List<MListButton> getButtons(Object value) {
        List<MListButton> result = new ArrayList<>();
        if (value instanceof OWLFrameSectionRow) {
            OWLFrameSectionRow frameRow = (OWLFrameSectionRow) value;
            OWLAxiom axiom = frameRow.getAxiom().getAxiomWithoutAnnotations();
            OWLReasoner reasoner = editorKit.getOWLModelManager().getReasoner();
            if (!editorKit.getOWLModelManager().getActiveOntology().containsAxiom(axiom) && !(reasoner != null && reasoner.isEntailed(axiom))) {
                result.add(acceptAxiomButton);
            }
            result.add(axiomAnnotationButton);
            axiomAnnotationButton.setAnnotationPresent(isAnnotationPresent(frameRow));
        }
        return result;
    }

    @Override
    public void dispose() {
        if (axiomAnnotationPanel != null) {
            axiomAnnotationPanel.dispose();
        }
        super.dispose();
    }

}
