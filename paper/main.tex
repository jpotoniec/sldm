% \documentclass{article}
% \usepackage[utf8]{inputenc}
% \usepackage{pgfplots}
% \title{Experimental setup}
% \author{Piotr Jakubowski }
% \date{May 2016}


% \usepackage{natbib}
% \usepackage{graphicx}
% \usetikzlibrary{trees}
% \pgfplotsset{every tick label/.append style={font=\small}}
% \begin{document}

% \maketitle

\section{Experimental evaluation\label{sec:experiment}}
This section presents all the steps that were undertaken in order to prepare and conduct an experiment on a crowdsourcing platform \name{CrowdFlower}\footnote{\url{https://www.crowdflower.com/}}.
Our aim was to answer the following research question: can SLDM mine new, meaningful axioms, that can be added to the ontology.
To answer the question, we used \name{DBpedia} 2015-04 with the \name{DBpedia} ontology and we followed the experimental protocol described below:
\begin{enumerate}
\item We conducted exploratory data analysis to select a set of classes.
\item For the selected classes, we used SLDM to generate superclass expressions, and used them to obtain a set of \owlSubClassOf{} axioms for a selected class, with the class in the left-hand side, and an expression in the right-hand side of an axiom.
\item We translated the generated axioms into natural language sentences.
\item We generated test questions to ensure that participants of the experiment are paying attention to their tasks.
\item These sentences were then posed to \name{CrowdFlower} for verification by the crowd.
\item We collected and analyzed the results of the verification.
\end{enumerate}

In the following sections, the details of the experimental protocol are explained.

\subsection{Exploratory data analysis}
To select a set of classes from the \name{DBpedia} ontology, what would allow us to conduct a high quality, statistically reliable experimental evaluation, we performed exploratory data analysis.
For every class in the ontology, we computed the following characteristics using \name{DBpedia} 2015-04:
\begin{enumerate}
\item the number of class instances,
\item the number of different triples, for which the subject belongs to the class,
\item the number of different predicates, for which there exists a triple in the dataset with a given predicate, and the subject belonging to the class,
\item the depth of the class in the subsumption hierarchy in the \name{DBpedia} ontology (the shortest path from the root of the hierarchy \texttt{owl:Thing} to the class).
\end{enumerate}
Histograms of the obtained values are presented in Figures \ref{fig:hist1}--\ref{fig:hist4}.
On the basis of the histograms, we chose a set of criteria that the selected classes should fulfill.

To provide enough statistical support, we chose classes with more that 1000 instances and every instance occurring as a subject on average in more than 50 triples.
To avoid generating a very large number of axioms, which would increase the costs of the crowdsourcing verification, we decided to keep the number of different predicates in range from 20 to 35.
Finally
%, to enable comparison of the results for a class and its ancestors, 
we decided on selecting classes with the depth at least 3, and in such a manner that all selected classes should have pairwise different parents and at least three different grandparents.

We selected 5 classes, which meet all the aforementioned criteria: \texttt{Journalist, ProgrammingLanguage, Book, MusicGenre, Crater} together with their ancestors: \texttt{Agent, Person, Work, Software, WrittenWork, TopicalConcept, Genre, Place, NaturalPlace}.
Full hierarchy is presented in Figure \ref{fig:hierarchy1}.
For each of these 14 classes, we used SLDM to generate two sets of axioms, the first one using the minimal support threshold $\theta_\sigma=.5$ and the second one with  $\theta_\sigma=.8$.
The obtained axioms are available in the \name{Git} repository \url{https://bitbucket.org/jpotoniec/sldm}, in the subfolder \texttt{CF\_source}

\begin{figure*}
\begin{subfigure}[t]{.48\textwidth}
\centering
\begin{tikzpicture}[font=\small]
    \begin{axis}[
      ybar,
      bar width=20pt,
      xlabel={Number of instances},
      ylabel={Number of classes},
      ymin=0,
      ytick=\empty,
      xtick=data,
      axis x line=bottom,
      axis y line=left,
      every axis x label/.style={at={(axis description cs:0.5,-0.35)}},
      enlarge x limits=0.2,
      xticklabels={$0-10$,$10-10^2$,$10^2-10^3$,$10^3-10^4$,$10^4-10^5$,$10^5-10^6$, $10^6-10^7$},
      xticklabel style={align=center, rotate=90},
      nodes near coords={\pgfmathprintnumber\pgfplotspointmeta}
    ]
      \addplot[fill=white] coordinates {
        (0,15) (10, 40) (20, 128) (30, 168) (40, 78) 
        (50, 18) (60, 2)
      };
    \end{axis}
  \end{tikzpicture}
    \caption{A histogram of number of class instances in \name{DBpedia} for the classes from the \name{DBpedia} ontology.\label{fig:hist1}}
\end{subfigure}
\begin{subfigure}[t]{.48\textwidth}
\centering
\begin{tikzpicture}[font=\small]
    \begin{axis}[
      ybar,
      bar width=15pt,
      xlabel={Number of subjects},
      ylabel={Number of classes},
      ymin=0,
      ytick=\empty,
      xtick=data,
      axis x line=bottom,
      axis y line=left,
      every axis x label/.style={at={(axis description cs:0.5,-0.25)}},
      enlarge x limits=0.05,
      xticklabels={0--10,10--2,20--30,30--40,40--50, 50--60, 60--70, 70--80, 80--90, 90--100, 100--110},
      xticklabel style={align=center, rotate=90},
      nodes near coords={\pgfmathprintnumber\pgfplotspointmeta}
    ]
      \addplot[fill=white] coordinates {
        (0,6) (10, 10) (20, 74) (30, 175) (40, 104) 
        (50, 41) (60, 12) (70, 8) (80, 4) (90, 7) (100, 2)
      };
    \end{axis}
  \end{tikzpicture}
  \caption{A histogram of average number of different predicates, for which there is a triple in  \name{DBpedia} with the subject from a given class.\label{fig:hist2}}
\end{subfigure}
\begin{subfigure}[t]{.48\textwidth}
\centering
\begin{tikzpicture}[font=\small]
    \begin{axis}[
      ybar,
      bar width=15pt,
      xlabel={Number of triples},
      ylabel={Number of classes},
      ymin=0,
      ytick=\empty,
      xtick=data,
      axis x line=bottom,
      axis y line=left,
      every axis x label/.style={at={(axis description cs:0.5,-0.3)}},
      enlarge x limits=0.2,
      xticklabels={0--100, 100--200, 200--300, 300--400, 400--500, 500--600, 600-700, 700--800},
      xticklabel style={align=center, rotate=90},
      nodes near coords={\pgfmathprintnumber\pgfplotspointmeta}
    ]
      \addplot[fill=white] coordinates {
        (0,57) (10, 233) (20, 89) (30, 30) (40, 11) 
        (50, 12) (60, 8) (70, 4)
      };
    \end{axis}
  \end{tikzpicture}
\caption{
A histogram of average number of triples in \name{DBpedia} for which the subject belongs to a given class.\label{fig:hist3}}
\end{subfigure}
\begin{subfigure}[t]{.48\textwidth}
\centering
\begin{tikzpicture}[font=\small]
    \begin{axis}[
      ybar,
      bar width=20pt,
      xlabel={Level of the class},
      ylabel={Number of classes},
      ymin=0,
      ytick=\empty,
      xtick=data,
      axis x line=bottom,
      axis y line=left,
      every axis x label/.style={at={(axis description cs:0.5,-0.11)}},
      enlarge x limits=0.2,
      xticklabels={1,2,3,4,5,6},
      xticklabel style={align=center},
      nodes near coords={\pgfmathprintnumber\pgfplotspointmeta}
    ]
      \addplot[fill=white] coordinates {
        (0,32) (10, 82) (20, 106) (30, 188) (40, 36) 
        (50, 5)
      };
    \end{axis}
  \end{tikzpicture}
\caption{Level of classes in the subsumption hierarchy in the \name{DBpedia} ontology.}
\label{fig:hist4}
\end{subfigure}
\caption{Histograms for characteristics used to select the set of classes from the \name{DBpedia} ontology to perform the experiment on.}
\end{figure*}


\begin{figure}
\centering
\tikzstyle{every node}=[draw=black,thick,anchor=west]

\begin{tikzpicture}[%
  grow via three points={one child at (0.5,-0.7) and
  two children at (0.5,-0.7) and (0.5,-1.4)},
  edge from parent path={(\tikzparentnode.south) |- (\tikzchildnode.west)}]
  \node[dashed] {\texttt{owl:Thing}}
    child { node {\texttt{dbo:Agent}}
      child { node {\texttt{dbo:Person}}
        child {node {\texttt{dbo:Journalist}}}
        }}
    child [missing]{}
    child { node {\texttt{dbo:Work}}
      child { node {\texttt{dbo:Software}}
        child {node {\texttt{dbo:Programminglanguage}}}}
      child [missing] {}
      child { node {\texttt{dbo:WrittenWork}}
        child {node {\texttt{dbo:Book}}}}
        }
    child [missing] {}
    child [missing] {}
    child [missing] {}
    child [missing] {}
    child { node {\texttt{dbo:TopicalConcept}}
      child { node {\texttt{dbo:Genre}}
        child {node {\texttt{dbo:MusicGenre}}}}
        }
    child [missing] {}
    child { node {\texttt{dbo:Place}}
      child { node {\texttt{dbo:NaturalPlace}}
        child {node {\texttt{dbo:Crater}}}}
        };
\end{tikzpicture}
\caption{The set of classes from the \name{DBpedia} ontology used in the experiment. 
\texttt{owl:Thing} is depicted in the picture only for reference and was not used in the experiment.}
\label{fig:hierarchy1}

\end{figure}
\subsection{Translation of the axioms to natural language}
%\jp{Słownik: axiom wychodzi z SLDM, sentence wychodzi z translacji, question to sentence z mozliwymi odpowiedzami do wyboru, czyli to, co wyswietla sie contributorowi na CF.}
Ontological axioms expressed in OWL (e.g. using Turtle) cannot be easily understood by English speakers that are not familiar with the Semantic Web  technologies.
Therefore we proposed a procedure of translation of OWL axioms to English.
We wanted to use a simple variant of the language, so we decided to choose Attempto Controlled English \citep{kaljurand2007verbalizing}.
It is a controlled version of normal English language that involves advantages of formal representation (well defined syntax, possibility of automatic processing) and natural language (expressiveness and ease of understanding) \citep{fuchs2008attempto}. 
Each person that knows basics of English is able to understand a translated sentence without any knowledge about its formal representation.
Usage of controlled language has one other, very important feature: the translation is fully reversible, so we do not lose any information. 

We decided to create a whole translator on our own.
The reason for that was the fact that existing tools (e.g. \name{OWL Verbalizer}\footnote{\url{https://github.com/Kaljurand/owl-verbalizer}} \citep{owlverb}) are restricted and work only for some of our examples.

In the OWL axioms generated by SLDM, we identified a set of structural templates and for every template we provided a corresponding template in English.
The URIs in the axioms were replaced by their corresponding labels during the translation.
The core idea of the translation tool is to analyze an axiom level by level and match it to the templates.
The output is a set of simple sentences, that represent more and more specific parts of constrains.
Sample axiom, that pertains \texttt{dbo:Journalist} class
\begin{alltt}
\texttt{dbo:Journalist} \owlSubClassOf{} \texttt{dbo:nationality} 
        \owlSome (\texttt{dbo:governmentType} \owlSome{} \texttt{owl:Thing}
        \owlAnd{} \texttt{dbo:leader} \owlSome \texttt{owl:Thing})
\end{alltt}
can be translated into sentences \textit{Every journalist has nationality. Nationality has government type. Nationality has leader.}
During the translation, we also performed some pruning, in order to make the final sentences more readable and limit the costs of the experiment.
We removed axioms that contained concepts which are characteristic for internal structure of \name{DBpedia} or act as metadata, e.g. predicate \texttt{dbp:hasPhotoCollection}.
We reason that such axioms are very hard to understand for a non-expert, and thus can not be efficiently verified by the crowd.
We also removed axioms containing namespaces from other Linked Data sets, e.g. \name{Wikidata} namespace, in order to decrease number of axioms to verify, and avoid displaying numerical URIs to the users, e.g. we removed the axiom \texttt{dbo:Book} \owlSubClassOf{} \texttt{wikidata:Q1930187}\footnote{The prefix \texttt{wikidata:} corresponds to \url{http://www.wikidata.org/entity/}}.

After application of the translation tool to the axioms generated by SLDM, we obtained a set of sentences.
Each of these sentences was then used as a base to form a question.
A question consists of: a sentence, which is to be verified; a set of three allowed answers, from which only one is to be selected: \emph{Yes}, \emph{No}, \emph{I don't know}; an optional field to explain why a particular answer was selected.

\subsection{Test questions}
The quality of the results achieved from crowdsourcing experiment can be significantly improved by introducing test questions \citep{mortensen2013crowdsourcing}.
The right answer to these questions is known before the experiment.
They are used to check reliability of crowdsourcing platform contributors.
They can be used in two ways:
\begin{enumerate}
\item One prepares a quiz for the contributors, that contains only the test questions.
If a contributor passes the test, she is allowed to answer payable questions.
\item For each set of questions, that are presented to a contributor, one question is a test question.
If the contributor does not answer the test question correctly, the other answers from her are discarded, and she does not get paid for them.
\end{enumerate}
During the experiment we used the second solution, because it requires contributor attention for every set of questions.
A good practice recommends having 10-20\% test questions in the input dataset\footnote{\url{http://www.success.crowdflower.com}}.
Some of our test questions were correct (i.e. required an answer \emph{Yes}) and some were incorrect (i.e. required an answer \emph{No}), in order to ensure that a contributor can not select always the same answer and ignore the questions completely.

To obtain the test questions, we used reasoner  \emph{Pellet} \cite{pellet} to find in the set of axioms generated by SLDM axioms that logically follows from the ontology.
The questions corresponding to these axioms were then used as the test questions with a known correct answer \emph{Yes}.

To obtain test questions with a correct \emph{No} answer, we selected some of the axioms generated by SLDM containing only a named class in the right-hand side and replaced the class by some other, unrelated class, obtaining e.g. an axiom \texttt{dbo:Journalist} \owlSome{} \texttt{dbo:Book}.
We also generated some false axioms by adding \owl{not} to the left-hand side of an axiom inferred from the ontology, obtaining e.g. a sentence \emph{Not every software is software}.


\subsection{CrowdFlower experiment setup}

The last activity to do before starting a crowdsourcing experiment is to setup the settings of the experiment and create an instruction for the contributors.
Both of these steps are crucial with respect to ensuring quality of the experiment results.

We set up the settings in the following way\footnote{See \url{https://success.crowdflower.com/hc/en-us/articles/201855719-Guide-to-Basic-Job-Settings-Page} for additional explanation of the settings}.
As our questions are quite simple, we requested for contributors of the lowest level, as this allowed us to obtain the results faster.
We decided on presenting 10 questions at once (i.e. on a single page) to a single contributor, as it should not take more than a few minutes to answer all of them.
We chose to pay 0.03 USD for answering one page of questions.
This is a typical payment on \emph{CrowdFlower} for the contributors of the lowest level, and should maintain their commitment.
We chose to request answers from 6 distinct contributors to one question.
In the preliminary experiments we requested only 3 answers, but in such a case a single disagreement (e.g. when a contributor does not understand a question) makes the result unreliable. 
On the other hand, we did not want to increase the number too much, to keep the costs under control.

An instruction for a crowdsourcing experiment should be as simple as possible, yet answer all questions a contributor can ask.
Moreover, it must contain examples of real questions, both positive and negative, and all steps that should be undertaken to solve them.
For our experiment, we inform the contributors, that axioms are represented as sentences and their task is to decide whether a given sentence is true, false or is not clear.
In the instruction we also mentioned one true sentence, one false sentence and one not clear sentence with an explanation in each case.
The full text of the instruction is also available in the \name{Git} repository.

\subsection{Experiment results}

For the crowdsourcing experiment we generated two sets of axioms, one with minimal support threshold $\theta_\sigma=0.5$ and the other one with the threshold $\theta_\sigma=0.8$.
The first set was translated to 425 payable questions and 61 test questions.
Each of the payable questions was asked to 6 distinct contributors, leading to 2559 answers.
This is more than expected $6\cdot425=2550$, because a few questions were answered by 7 contributors.
According to \name{CrowdFlower} documentation\footnote{\url{https://success.crowdflower.com/hc/en-us/articles/201855849-Judgment-Overcollection-FAQ}} it is probably because some of the contributors were first considered by \name{CrowdFlower} to be not trusted, and only later became trusted.
The second set was translated to 168 payable questions and 56 test questions, the payable questions yielded $6\cdot 168=1008$ answers.
Reliability of all contributors was checked with the test questions and when it dropped below 70\% (more than 30\% of the presented test questions had wrong answers) they were refused to continue answering the questions.

A summary of the results is presented in Table \ref{tab:cf-summary}.
In the first set $28.71\%$ (resp. $55.35\%$ in the second set) of axioms were indisputably accepted by the crowd.
If we allow for a single disagreement or misunderstanding from the contributors, $66.82\%$ (resp. $80.36\%$) of the axioms were accepted.
Our aim was to verify if SLDM can mine new, meaningful axioms, that can be added to the ontology.
We find both results to answer positively to the question.
All the verified knowledge was new, because the axioms that could be inferred from the ontology were used as the test questions.
In both cases over $50\%$ of the axioms were accepted by the crowd, meaning that most of the effort of an ontology engineer verifying the mined axioms goes to accepting rather than rejecting the axioms.

\begin{table}
\caption{A summary of the results of the crowdsourcing experiment.
Each row corresponds to a group of questions with the same numbers of \emph{Yes}, \emph{No} and \emph{I don't know} answers, as given by the first three columns of the table.
In the last two columns specified are numbers of questions in the group, as an absolute number and relatively to the overall number of questions in the respective set.
For example, 119 questions (i.e. $28\%$) from the first set and 93 questions (i.e. $55.35\%$) from the second set were given answer \emph{Yes} by all six contributors.
The row \emph{accepted} is a summary for the rows with at most one \emph{No} or \emph{I don't know} answer, and the row \emph{rejected} is a summary for the rest of the rows.
\label{tab:cf-summary}
}
\centering
\begin{tabular}{cc>{\centering\arraybackslash}p{1cm}|rr|rr}
\emph{Yes} & \emph{No} & \emph{I don't know} & \multicolumn{2}{c|}{$\theta_\sigma=0.5$} & \multicolumn{2}{c}{$\theta_\sigma=0.8$} \\
\hline
1 & 3 & 2 & & & 1 & $0.60\%$ \\
1 & 4 & 1 & 1 & $0.24\%$ \\
1 & 5 & 0 & 2 & $0.47\%$ \\
2 & 1 & 3 & & & 1 & $0.60\%$ \\
2 & 2 & 2 & 5 & $1.18\%$ \\
2 & 3 & 1 & & & 3 & $1.79\%$ \\
2 & 4 & 0 & 6 & $1.41\%$ & 1 & $0.60\%$ \\
3 & 0 & 3 & 2 & $0.47\%$ \\
3 & 1 & 2 & 10 & $2.35\%$ \\
3 & 2 & 1 & 17 & $4.00\%$ & 4 & $2.38\%$ \\
3 & 3 & 0 & 3 & $0.71\%$ & 2 & $1.19\%$ \\
3 & 4 & 0 & 1 & $0.24\%$ \\
4 & 0 & 2 & 21 & $4.94\%$ & 2 & $1.19\%$ \\
4 & 1 & 1 & 44 & $10.35\%$ & 12 & $7.14\%$ \\
4 & 2 & 0 & 27 & $6.35\%$ & 7 & $4.17\%$ \\
5 & 2 & 0 & 2 & $0.47\%$ \\
\hline
5 & 0 & 1 & 79 & $18.59\%$ & 9 & $5.35\%$ \\
5 & 1 & 0 & 80 & $18.82\%$ & 33 & $19.64\%$ \\
6 & 0 & 0 & 119 & $28.00\%$ & 93 & $55.35\%$ \\
6 & 1 & 0 & 3 & $0.71\%$ \\
7 & 0 & 0 & 3 & $0.71\%$ \\
\hline
\hline
\multicolumn{3}{c|}{rejected} & 141 & $33.18\%$ & 33 & $19.64\%$ \\
\multicolumn{3}{c|}{accepted} & 284 & $66.82\%$ & 135 & $80.36\%$ \\
\multicolumn{3}{c|}{overall} & 425 & $100.00\%$ & 168 & $100.00\%$ \\
\end{tabular}
\end{table}

The questions having at most one \emph{Yes} answer are presented in Table \ref{tab:cf-very-wrong}.
The first two axioms do not make much sense, as the properties \texttt{dbp:color} and \texttt{dbp:bgcolor} are probably an artifact of the extraction process from \name{Wikipedia} to \name{DBpedia}.
The third axiom is clearly due to the low minimal support threshold for the set, as there are 1567 URIs asserted to belong to the class \texttt{dbo:TopicalConcept}, out of which 1096 (i.e. $70\%$) belong also to the class \texttt{dbo:MusicalGenre}.
Finally, the last axiom would probably be accepted by the contributors if its verbalization was better.


\begin{table*}
\caption{The questions along with the axioms they were translated from, that were accepted by at most one contributor.
The first three questions are from the set with $\theta_\sigma=0.5$, and the last one from the other set.
Prefix \texttt{geo:} corresponds to \texttt{http://www.w3.org/2003/01/geo/wgs84\_pos\#}.
\label{tab:cf-very-wrong}}
\centering
\begin{tabular}{p{.55\textwidth}p{.40\textwidth}}
axiom & verbalization \\
\hline
\texttt{dbo:Genre} \owlSubClassOf{} \texttt{dbp:color} \owlSome{} \texttt{rdf:PlainLiteral} & Every genre has color. color is Literal. \\
\texttt{dbo:Genre} \owlSubClassOf{} \texttt{dbp:bgcolor} \owlSome{} \texttt{rdf:PlainLiteral} & Every genre has bgcolor. bgcolor is Literal. \\
\texttt{dbo:TopicalConcept} \owlSubClassOf{} \texttt{dbo:MusicGenre} & Every topical concept is music genre. \\
\hline
%(http://www.w3.org/2003/01/geo/wgs84_pos#long >= "-178.95"^^<http://www.w3.org/2001/XMLSchema#float>)
%XXX uwaga, w pliku bylo float, ale my tego typu nie obslugujemy, wiec podmienilem na decimal
\texttt{dbo:Place} \owlSubClassOf{} \texttt{geo:long} \owlSome{} \texttt{xsd:decimal[>=-178.95]}
& Every place (Immobile things or locations.) has long *. long * is floating number greater/more than $-178.95$.
% %.5
% (1, 5, 0) Genre1.txt Constrain_1 "Every genre has color. color is Literal."
% (1, 4, 1) Genre1.txt Constrain_4 "Every genre has bgcolor. bgcolor is Literal."
% (1, 5, 0) Topicalconcept1.txt Constrain_9 "Every topical concept is music genre."
% %.8
% (1, 3, 2) Place1.txt Constrain_3 "Every place (Immobile things or locations.) has long *. long * is floating number greater/more than -178.95."
\end{tabular}
\end{table*}


In the set of the questions accepted by all of the asked contributors, there was for example the following quite complex sentence: \emph{Every written work (Written work is any text written to read it (e.g. -  books, newspaper, articles)) has author. author has birth year. birth year is non-negative integer.}.
The sentence originated from the axiom \texttt{dbo:WrittenWork} \owlSubClassOf{} \texttt{dbo:author} \owlSome{} (\texttt{dbo:birthYear} \owlSome{} \texttt{xsd:nonNegativeInteger}).

