package put.sldm.desktop;

import java.util.NoSuchElementException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import put.sldm.FrequentPatterns;
import put.sldm.FrequentPatternsProcessor;
import put.sldm.SLDM;
import put.sldm.SPARQL;
import put.sldm.config.DesktopConfig;
import put.sldm.config.FileConfig;

public class BatchRunner {

    private final static Logger logger = Logger.getLogger(SLDM.class);

    public static void main(String[] args) {
        try {
            DesktopConfig cfg = new FileConfig(args[0]);
            SPARQL sparql = new SPARQL(cfg);
            SLDM sldm = new SLDM(cfg, sparql);
            FrequentPatternsProcessor proc = new FrequentPatternsProcessor();
            sldm.run(false, proc);
            FrequentPatterns patterns = proc.deliver();
            new SaveCSV().saveCSV(patterns, cfg.getCSVFile());
            if (cfg.getRDFFile() != null) {
                new SaveRDF().save(patterns, cfg.getRDFFile(), cfg);
            }
            System.err.println("Interactions with SPARQL endpoint: " + SPARQL.getQueryCounter());
        } catch (ConfigurationException | NoSuchElementException ex) {
            logger.error("main", ex);
        }
    }
}
