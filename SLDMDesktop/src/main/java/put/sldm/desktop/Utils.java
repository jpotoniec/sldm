package put.sldm.desktop;

import com.hp.hpl.jena.datatypes.xsd.XSDDateTime;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.sparql.serializer.SerializationContext;
import com.hp.hpl.jena.sparql.util.FmtUtils;
import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Map;
import java.util.jar.Manifest;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

public class Utils {

    private static final Logger logger = Logger.getLogger(put.sldm.Utils.class);
    private static final Model m = ModelFactory.createDefaultModel();
    private static final SerializationContext emptyContext = new SerializationContext();

    public static Literal createTypedLiteral(double value) {
        synchronized (m) {
            return m.createTypedLiteral(value);
        }
    }

    public static Literal createTypedLiteral(Calendar value) {
        synchronized (m) {
            return m.createTypedLiteral(value);
        }
    }

    public static String toString(RDFNode node) {
        //synchronized ?
        return FmtUtils.stringForRDFNode(node, emptyContext);
    }

    public static IRI getMyName() {
        try {
            Enumeration<URL> resources = put.sldm.Utils.class.getClassLoader()
                    .getResources("META-INF/MANIFEST.MF");
            while (resources.hasMoreElements()) {
                try (InputStream stream = resources.nextElement().openStream()) {
                    Manifest m = new Manifest(stream);
                    for (Map.Entry<Object, Object> e : m.getMainAttributes().entrySet()) {
                        if ("IRI".equals(e.getKey().toString())) {
                            return IRI.create(e.getValue().toString());
                        }
                    }
                } catch (Exception ex) {
                    logger.error("looking for manifest", ex);
                }
            }
        } catch (Exception ex) {
            logger.error("looking for manifest (2)", ex);
        }
        return IRI.create("http://semantic.cs.put.poznan.pl/leolod/SLDM");
    }

    public static OWLLiteral dateTimeLiteral(OWLDataFactory factory, GregorianCalendar cal) {
        return factory.getOWLLiteral(new XSDDateTime(cal).toString(), OWL2Datatype.XSD_DATE_TIME);
    }
}
