package put.sldm.desktop;

import java.io.IOException;
import java.io.PrintStream;
import org.apache.log4j.Logger;
import put.sldm.FrequentPatterns;
import put.sldm.patterns.DefaultVariableGenerator;
import put.sldm.patterns.FullPattern;

public class SaveCSV {

    private final static Logger logger = Logger.getLogger(SaveCSV.class);

    public void saveCSV(FrequentPatterns patterns, String file) {
        try (PrintStream output = new PrintStream(file)) {
            output.println("\"Pattern\";\"Support\";\"Relative support\";\"Estimate\";\"SPARQL\"");
            for (FullPattern p : patterns.getPatterns()) {
                String sparqlPatterns = p.toSPARQL("?x", new DefaultVariableGenerator());
                int q = p.getQuality();
                output.printf("\"%3$s\";\"%2$d\";\"%4$f\";\"%5$f\";\"%1$s\"\n", sparqlPatterns.replace("\n", " "), q, p.toString(), 1.0 * q / patterns.getMaxQuality(), p.estimateQuality());
            }
        } catch (IOException ex) {
            logger.error("Saving CSV", ex);
        }
    }
}
