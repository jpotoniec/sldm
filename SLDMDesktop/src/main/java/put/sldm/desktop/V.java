package put.sldm.desktop;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.vocab.Namespaces;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

public class V {

    public static final String PROV = "http://www.w3.org/ns/prov#";

    public final OWLDataFactory factory;
    public final OWLClass SENTENCE;
    public final OWLAnnotationProperty SUBJECT;
    public final OWLAnnotationProperty PREDICATE;
    public final OWLAnnotationProperty OBJECT;
    public final OWLClass ENTITY;
    public final OWLClass ACTIVITY;
    public final OWLDataProperty provGeneratedAtTime, STARTED_AT_TIME, ENDED_AT_TIME;
    public final OWLObjectProperty WAS_ATTRIBUTE_TO;
    public final OWLObjectProperty WAS_GENERATED_BY, WAS_ASSOCIATED_WITH;
    public final OWLClassExpression DELEGATION;
    public final OWLObjectPropertyExpression HAD_ACTIVITY;
    public final OWLObjectPropertyExpression AGENT;
    public final OWLObjectPropertyExpression QUALIFIED_DELEGATION;
    public final OWLClassExpression SOFTWARE_AGENT;

    public V(OWLDataFactory factory) {
        this.factory = factory;
        SENTENCE = factory.getOWLClass(IRI.create(Namespaces.RDF.toString(), "Statement"));
        SUBJECT = factory.getOWLAnnotationProperty(OWLRDFVocabulary.RDF_SUBJECT.getIRI());
        PREDICATE = factory.getOWLAnnotationProperty(OWLRDFVocabulary.RDF_PREDICATE.getIRI());
        OBJECT = factory.getOWLAnnotationProperty(OWLRDFVocabulary.RDF_OBJECT.getIRI());
        ENTITY = factory.getOWLClass(IRI.create(PROV, "Entity"));
        ACTIVITY = factory.getOWLClass(IRI.create(PROV, "Activity"));
        DELEGATION = factory.getOWLClass(IRI.create(PROV, "Delegation"));
        provGeneratedAtTime = factory.getOWLDataProperty(IRI.create(PROV, "generatedAtTime"));
        STARTED_AT_TIME = factory.getOWLDataProperty(IRI.create(PROV, "startedAtTime"));
        ENDED_AT_TIME = factory.getOWLDataProperty(IRI.create(PROV, "endedAtTime"));
        WAS_ATTRIBUTE_TO = factory.getOWLObjectProperty(IRI.create(PROV, "wasAttributedTo"));
        WAS_GENERATED_BY = factory.getOWLObjectProperty(IRI.create(PROV, "wasGenerateBy"));
        WAS_ASSOCIATED_WITH = factory.getOWLObjectProperty(IRI.create(PROV, "wasAssociatedWith"));
        AGENT = factory.getOWLObjectProperty(IRI.create(PROV, "agent"));
        HAD_ACTIVITY = factory.getOWLObjectProperty(IRI.create(PROV, "hadActivity"));
        QUALIFIED_DELEGATION = factory.getOWLObjectProperty(IRI.create(PROV, "qualifiedDelegation"));
        SOFTWARE_AGENT = factory.getOWLClass(IRI.create(PROV, "SoftwareAgent"));
    }
}
