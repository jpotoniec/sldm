package put.sldm.desktop;

import java.util.NoSuchElementException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxOWLObjectRendererImpl;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxObjectRenderer;
import org.semanticweb.owlapi.model.OWLClassExpression;
import put.sldm.FrequentPatterns;
import put.sldm.SLDM;
import put.sldm.SLDMListener;
import put.sldm.SPARQL;
import put.sldm.config.DesktopConfig;
import put.sldm.config.FileConfig;
import put.sldm.patterns.FullPattern;
import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;

public class Runner {

    private final static Logger logger = Logger.getLogger(SLDM.class);

    public static void main(String[] args) {
        try {
            DesktopConfig cfg = new FileConfig(args.length >= 1 ? args[0] : "astronaut.properties");
            SPARQL sparql = new SPARQL(cfg);
            SLDM sldm = new SLDM(cfg, sparql);
            final OWLDataFactoryImpl f = new OWLDataFactoryImpl();
            final ManchesterOWLSyntaxOWLObjectRendererImpl rend = new ManchesterOWLSyntaxOWLObjectRendererImpl();
            sldm.run(false, new SLDMListener() {

                @Override
                public void miningStarted() {
                    System.out.println("Mining started");
                }

                @Override
                public void baseDetermined(int size) {
                    System.out.printf("Base size: %d items\n", size);
                }

                @Override
                public void miningFinished() {
                }

                @Override
                public void patternMined(FullPattern p) {
                    OWLClassExpression expr = p.createClass(f);
                    String text = rend.render(expr);
                    System.out.printf("%s %f\n", text, p.estimateQuality());
                }

                @Override
                public boolean shouldStop() {
                    return false;
                }

                @Override
                public void stateChanged(SLDMListener.State current) {
//                    System.out.println(current);
                }
            });
        } catch (ConfigurationException | NoSuchElementException ex) {
            logger.error("main", ex);
        }
    }
}
