package put.sldm.desktop;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import put.sldm.FrequentPatterns;
import put.sldm.config.DesktopConfig;
import static put.sldm.desktop.Utils.*;
import put.sldm.patterns.FullPattern;

public class SaveRDF {
    
    private final static Logger logger = Logger.getLogger(SaveRDF.class);
    
       public void save(FrequentPatterns patterns,String file, DesktopConfig cfg) {
        String prefix = String.format("http://semantic.cs.put.poznan.pl/leolod/SLDM/executions/%s/", UUID.randomUUID());
        try {
            OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
            OWLDataFactory factory = manager.getOWLDataFactory();
            OWLOntology o = manager.createOntology();
            OWLClass baseClass = factory.getOWLClass(cfg.getBaseClass());
            V V = new V(factory);
            OWLNamedIndividual me = factory.getOWLNamedIndividual(getMyName());
            manager.addAxiom(o, factory.getOWLClassAssertionAxiom(V.SOFTWARE_AGENT, me));
            OWLNamedIndividual user = factory.getOWLNamedIndividual(cfg.getUserIRI());
            OWLNamedIndividual execution = factory.getOWLNamedIndividual(IRI.create(prefix, "execution"));
            manager.addAxiom(o, factory.getOWLClassAssertionAxiom(V.ACTIVITY, execution));
//            manager.addAxiom(o, factory.getOWLDataPropertyAssertionAxiom(V.STARTED_AT_TIME, execution, Utils.dateTimeLiteral(factory, patterns.getBegin())));
//            manager.addAxiom(o, factory.getOWLDataPropertyAssertionAxiom(V.ENDED_AT_TIME, execution, Utils.dateTimeLiteral(factory, patterns.getEnd())));
            manager.addAxiom(o, factory.getOWLObjectPropertyAssertionAxiom(V.WAS_ASSOCIATED_WITH, execution, me));
            OWLNamedIndividual delegation = factory.getOWLNamedIndividual(IRI.create(prefix, "delegation"));
            manager.addAxiom(o, factory.getOWLClassAssertionAxiom(V.DELEGATION, delegation));
            manager.addAxiom(o, factory.getOWLObjectPropertyAssertionAxiom(V.AGENT, delegation, user));
            //manager.addAxiom(o, factory.getOWLClassAssertionAxiom(V.HAD_ROLE, V.RUNNER));
            manager.addAxiom(o, factory.getOWLObjectPropertyAssertionAxiom(V.HAD_ACTIVITY, delegation, execution));
            manager.addAxiom(o, factory.getOWLObjectPropertyAssertionAxiom(V.QUALIFIED_DELEGATION, me, delegation));
            int n = 1;
            for (FullPattern p : patterns.getPatterns()) {
                IRI name = IRI.create(prefix, String.format("Constrain_%d", n));
                IRI reifName = IRI.create(prefix, String.format("reified_statement_%d", n));
                n++;
                OWLClassExpression ex = p.createClass(factory);
                manager.addAxiom(o, factory.getOWLSubClassOfAxiom(baseClass, ex));
                OWLClass constraint = factory.getOWLClass(name);
                manager.addAxiom(o, factory.getOWLEquivalentClassesAxiom(constraint, ex));
                OWLAnnotation ann = factory.getOWLAnnotation(factory.getOWLAnnotationProperty(OWLRDFVocabulary.RDFS_COMMENT.getIRI()), factory.getOWLLiteral(p.toString()));
                manager.addAxiom(o, factory.getOWLAnnotationAssertionAxiom(constraint.getIRI(), ann));
                Set<OWLAxiom> changes = new HashSet<>();
                OWLNamedIndividual entity = factory.getOWLNamedIndividual(reifName);
                changes.add(factory.getOWLClassAssertionAxiom(V.SENTENCE, entity));
                changes.add(factory.getOWLAnnotationAssertionAxiom(V.SUBJECT, entity.getIRI(), baseClass.getIRI()));
                changes.add(factory.getOWLAnnotationAssertionAxiom(V.PREDICATE, entity.getIRI(), OWLRDFVocabulary.RDFS_SUBCLASS_OF.getIRI()));
                changes.add(factory.getOWLAnnotationAssertionAxiom(V.OBJECT, entity.getIRI(), name));
                changes.add(factory.getOWLClassAssertionAxiom(V.ENTITY, entity));
                changes.add(factory.getOWLObjectPropertyAssertionAxiom(V.WAS_ATTRIBUTE_TO, entity, me));
                changes.add(factory.getOWLObjectPropertyAssertionAxiom(V.WAS_GENERATED_BY, entity, execution));
                manager.addAxioms(o, changes);
            }
            manager.saveOntology(o, IRI.create(new File(file)));
        } catch (OWLOntologyCreationException | OWLOntologyStorageException ex) {
            logger.error("Saving ontology", ex);
        }
    }

}
