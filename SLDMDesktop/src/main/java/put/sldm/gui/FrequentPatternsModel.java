package put.sldm.gui;

import javax.swing.table.AbstractTableModel;
import org.semanticweb.owlapi.dlsyntax.renderer.DLSyntaxObjectRenderer;
import org.semanticweb.owlapi.manchestersyntax.renderer.ManchesterOWLSyntaxOWLObjectRendererImpl;
import org.semanticweb.owlapi.model.OWLClassExpression;
import put.sldm.FrequentPatterns;
import put.sldm.patterns.FullPattern;
import uk.ac.manchester.cs.owl.owlapi.OWLDataFactoryImpl;

class FrequentPatternsModel extends AbstractTableModel {

    public static final String[] LABELS = {
        "Pattern",
        "Support",
        "Relative support",
        "Relative support estimation",
        "DL representation",
        "Internal representation"
    };

    private final FrequentPatterns patterns;

    public FrequentPatternsModel(FrequentPatterns result) {
        this.patterns = result;
    }

    @Override
    public int getRowCount() {
        return patterns.getPatterns().size();
    }

    @Override
    public String getColumnName(int column) {
        return LABELS[column];
    }

    @Override
    public int getColumnCount() {
        return LABELS.length;
    }

    private String toDL(FullPattern p) {
        OWLClassExpression cl = p.createClass(new OWLDataFactoryImpl());
        return new DLSyntaxObjectRenderer().render(cl);
    }

    private String toManchester(FullPattern p) {
        OWLClassExpression cl = p.createClass(new OWLDataFactoryImpl());
        return new ManchesterOWLSyntaxOWLObjectRendererImpl().render(cl);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        FullPattern p = patterns.getPatterns().get(rowIndex);
        switch (columnIndex) {
            case 0:
                return toManchester(p);
            case 1:
                return p.getQuality();
            case 2:
                return ((double) p.getQuality()) / patterns.getMaxQuality();
            case 3:
                return p.estimateQuality();
            case 4:
                return toDL(p);
            case 5:
                return p.toString();
            default:
                throw new IllegalArgumentException();
        }
    }

    public FrequentPatterns getPatterns() {
        return patterns;
    }

}
