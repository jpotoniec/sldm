package put.sldm.config;

import org.semanticweb.owlapi.model.IRI;

public interface DesktopConfig extends Config {

    public boolean getAddNamedClasses();

    public String getCSVFile();

    public String getRDFFile();

    public IRI getBaseClass();

    public IRI getUserIRI();

}
