package put.sldm.config;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.configuration.AbstractConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.semanticweb.owlapi.model.IRI;
import put.sldm.sampling.SamplingStrategyFactory;

public class FileConfig implements DesktopConfig {

    private static final String ADD_NAMED_CLASSES = "add_named_classes";
    private static final String REMOTE_VALIDATION = "remote_validation";
    private static final String OUTPUT_FILE_NAME = "output_file_name";
    private static final String RDF_FILE_NAME = "rdf_file_name";
    private static final String IGNORED_PROPERTIES = "ignored_properties";
    private static final String MAX_LEVEL = "max_level";
    private static final String MAX_OPEN_LEVEL = "max_open_level";
    private static final String MAX_VALUES_SIZE = "max_values_size";
    private static final String INITIAL_QUERY = "initial_query";
    private static final String ENDPOINT = "endpoint";
    private static final String MIN_FUNCTIONAL_SUPPORT = "min_functional_support";
    private static final String MIN_SUPPORT = "min_support";
    private static final String USER_IRI = "user_iri";
    private static final String BASE_CLASS = "base_class";
    private static final String SAMPLE_SIZE = "sample_size";
    private static final String SAMPLING_STRATEGY = "sampling_strategy";
    private static final String RANDOM_SEED = "random_seed";

    private final double minSupport;
    private final double minFunctionalSupport;
    private final String endpoint;
    private final String initialQuery;
    private final int maxValuesSize;
    private final int maxOpenLevel;
    private final int maxLevel;
    private final List<Pattern> ignoredProperties;
    private final String outputFileName;
    private final String rdfFileName;
    private final boolean remoteValidation;
    private final boolean add_named_classes;
    private final IRI userIRI;
    private final IRI baseClass;
    private final int sampleSize;
    private final Long randomSeed;
    private SamplingStrategyFactory.SamplingStrategyId samplingStrategy;

    public FileConfig(AbstractConfiguration cfg) {
        minSupport = cfg.getDouble(MIN_SUPPORT);
        minFunctionalSupport = cfg.getDouble(MIN_FUNCTIONAL_SUPPORT);
        endpoint = cfg.getString(ENDPOINT);
        initialQuery = cfg.getString(INITIAL_QUERY);
        maxValuesSize = cfg.getInt(MAX_VALUES_SIZE);
        maxOpenLevel = cfg.getInt(MAX_OPEN_LEVEL);
        maxLevel = cfg.getInt(MAX_LEVEL);
        ignoredProperties = new ArrayList<>();
        for (Object p : cfg.getList(IGNORED_PROPERTIES)) {
            ignoredProperties.add(Pattern.compile(p.toString()));
        }
        outputFileName = cfg.getString(OUTPUT_FILE_NAME);
        rdfFileName = cfg.getString(RDF_FILE_NAME, null);
        remoteValidation = cfg.getBoolean(REMOTE_VALIDATION, false);
        add_named_classes = cfg.getBoolean(ADD_NAMED_CLASSES, false);
        userIRI = IRI.create(cfg.getString(USER_IRI));
        baseClass = IRI.create(cfg.getString(BASE_CLASS));
        sampleSize = cfg.getInt(SAMPLE_SIZE, 0);
        randomSeed = cfg.getLong(RANDOM_SEED, null);
        samplingStrategy = SamplingStrategyFactory.SamplingStrategyId.valueOf(cfg.getString(SAMPLING_STRATEGY, SamplingStrategyFactory.SamplingStrategyId.UNIFORM.toString()));
    }

    public FileConfig(String file) throws ConfigurationException {
        this(new PropertiesConfiguration(file));
    }

    public FileConfig(File file) throws ConfigurationException {
        this(new PropertiesConfiguration(file));
    }

    @Override
    public double getMinSupport() {
        return minSupport;
    }

    @Override
    public double getMinFunctionalSupport() {
        return minFunctionalSupport;
    }

    @Override
    public String getEndpoint() {
        return endpoint;
    }

    @Override
    public String getInitialQuery() {
        return initialQuery;
    }

    @Override
    public int getMaxValuesSize() {
        return maxValuesSize;
    }

    @Override
    public int getMaxOpenLevel() {
        return maxOpenLevel;
    }

    @Override
    public int getMaxLevel() {
        return maxLevel;
    }

    @Override
    public List<Pattern> getIgnoredProperties() {
        return ignoredProperties;
    }

    @Override
    public String getCSVFile() {
        return outputFileName;
    }

    @Override
    public boolean getRemoteValidation() {
        return remoteValidation;
    }

    @Override
    public boolean getAddNamedClasses() {
        return add_named_classes;
    }

    @Override
    public IRI getUserIRI() {
        return userIRI;
    }

    @Override
    public IRI getBaseClass() {
        return baseClass;
    }

    public static PropertiesConfiguration export(DesktopConfig config) {
        PropertiesConfiguration cfg = new PropertiesConfiguration();
        cfg.setProperty(MIN_SUPPORT, config.getMinSupport());
        cfg.setProperty(MIN_FUNCTIONAL_SUPPORT, config.getMinFunctionalSupport());
        cfg.setProperty(ENDPOINT, config.getEndpoint());
        cfg.setProperty(INITIAL_QUERY, config.getInitialQuery());
        cfg.setProperty(MAX_VALUES_SIZE, config.getMaxValuesSize());
        cfg.setProperty(MAX_OPEN_LEVEL, config.getMaxOpenLevel());
        cfg.setProperty(MAX_LEVEL, config.getMaxLevel());
        List<String> ignored = new ArrayList<>();
        for (Pattern p : config.getIgnoredProperties()) {
            ignored.add(p.toString());
        }
        cfg.setProperty(IGNORED_PROPERTIES, ignored);
        cfg.setProperty(OUTPUT_FILE_NAME, config.getCSVFile());
        cfg.setProperty(REMOTE_VALIDATION, config.getRemoteValidation());
        cfg.setProperty(ADD_NAMED_CLASSES, config.getAddNamedClasses());
        cfg.setProperty(USER_IRI, config.getUserIRI().toString());
        cfg.setProperty(BASE_CLASS, config.getBaseClass().toString());
        if (config.isSamplingEnabled()) {
            cfg.setProperty(SAMPLE_SIZE, config.getSampleSize());
            cfg.setProperty(SAMPLING_STRATEGY, config.getSamplingStrategy().toString());
        }
        cfg.setProperty(RANDOM_SEED, config.getRandomSeed());
        return cfg;
    }

    public static void save(DesktopConfig config, File file) throws ConfigurationException {
        export(config).save(file);
    }

    @Override
    public String getRDFFile() {
        return rdfFileName;
    }

    @Override
    public boolean isSamplingEnabled() {
        return sampleSize > 0;
    }

    @Override
    public SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy() {
        return samplingStrategy;
    }

    @Override
    public int getSampleSize() {
        return sampleSize;
    }

    @Override
    public Long getRandomSeed() {
        return randomSeed;
    }

}
