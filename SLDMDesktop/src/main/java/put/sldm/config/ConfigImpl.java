package put.sldm.config;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import org.semanticweb.owlapi.model.IRI;
import put.sldm.sampling.SamplingStrategyFactory;

public class ConfigImpl implements DesktopConfig {

    @Override
    public double getMinSupport() {
        return .9;
    }

    @Override
    public double getMinFunctionalSupport() {
        return getMinSupport() / 5;
    }

    @Override
    public String getEndpoint() {
        return "http://10.0.8.2:8890/sparql";
    }

    @Override
    public String getInitialQuery() {
        return "select distinct ?s where {?s a <http://dbpedia.org/ontology/Astronaut>} limit 200";
    }

    @Override
    public int getMaxValuesSize() {
        return 25;
    }

    @Override
    public int getMaxOpenLevel() {
        return 1;
    }

    @Override
    public int getMaxLevel() {
        return 1;
    }

    @Override
    public List<Pattern> getIgnoredProperties() {
        return Arrays.asList(
                Pattern.compile("http://dbpedia.org/ontology/wikiPageID"),
                Pattern.compile("http://dbpedia.org/ontology/wikiPageRevisionID"),
                Pattern.compile("http://www.w3.org/2002/07/owl#sameAs")
        );
    }

    @Override
    public String getCSVFile() {
        return "/tmp/patterns.csv";
    }

    @Override
    public boolean getRemoteValidation() {
        return false;
    }

    @Override
    public boolean getAddNamedClasses() {
        return true;
    }

    @Override
    public IRI getBaseClass() {
        return IRI.create("http://www.example.com/foo#BaseClass");
    }

    @Override
    public IRI getUserIRI() {
        return IRI.create("http://www.cs.put.poznan.pl/jpotoniec/foaf.me");
    }

    @Override
    public String getRDFFile() {
        return null;
    }

    @Override
    public boolean isSamplingEnabled() {
        return false;
    }

    @Override
    public SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy() {
        return SamplingStrategyFactory.SamplingStrategyId.UNIFORM;
    }

    @Override
    public int getSampleSize() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Long getRandomSeed() {
        return null;
    }

}
