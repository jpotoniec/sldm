# LeoLOD Swift Linked Data Miner plugin for Protege #

A full description of SLDM is available in the paper *Swift Linked Data Miner: Mining OWL 2 EL class expressions directly from online RDF datasets* (J. Potoniec, P. Jakubowski, A. Lawrynowicz; Journal of Web Semantics, Volumes 46–47, Pages 31-50) available at [http://www.sciencedirect.com/science/article/pii/S157082681730032X](http://www.sciencedirect.com/science/article/pii/S157082681730032X). The preprint is avaialable at [https://arxiv.org/abs/1710.07114](https://arxiv.org/abs/1710.07114).


### Obtaining the plugin ###

The current version of the plugin (for OWL 2 EL) is available at [https://semantic.cs.put.poznan.pl/sldm/SLDMProtege-2018-01-31.jar](https://semantic.cs.put.poznan.pl/sldm/SLDMProtege-2018-01-31.jar).
An older version is also available from the [LeoLOD website](http://www.cs.put.poznan.pl/alawrynowicz/leolod).
You can also compile the plugin by your own:

```
#!shell

git clone https://bitbucket.org/jpotoniec/sldm.git
cd sldm
mvn package
```
Maven will download all required dependencies and build a JAR file with the plugin for you in `SLDMProtege/target/SLDMProtege-1.0-SNAPSHOT.jar`

### Installing the plugin ###
Copy the JAR file to the subdirectory `plugins` of the Protege main directory

### Using the plugin ###

1. Load an ontology you want to extend to Protege
2. Enable SLDM tab: click `Window→Tabs→SLDM tab`
3. Go to the SLDM tab.
4. In the left side of the window, select a class you want to extend with additional axioms.
5. Fill the filed `SPARQL endpoint` in the middle of the screen.
6. Click the button `Run`. SLDM will now perform a datamining on the SPARQL endpoint you specified to discover statisticaly significant axioms about the class you selected.
8. In the center of the window, there will be a list of mined axioms. You can click on a button with `@` symbol to see provenance metadata for the axiom and on a button with `✓` symbol to add the axiom to the ontology. This second button is available only for axioms that are not already present in the ontology.

Below, you can see axioms mined for a DBpedia class Astronaut.
![sldm2.png](https://bitbucket.org/repo/aXzGxk/images/1899447821-sldm2.png)