package put.sldm.rules;

import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import put.sldm.Weight;
import put.sldm.config.Config;
import put.sldm.patterns.partial.DatatypePropertyPattern;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.rules.AbstractRule;
import put.sldm.tiny.TinyLiteral;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class DatatypeRule extends AbstractRule {

    public DatatypeRule(Config cfg) {
        super(cfg);
    }

//    @Override
//    public void find(Counter<TinyNode> objects, TinyResource p, List<PartialPattern> patterns, Counter<TinyNode> I, Subgraph g) {
//        Counter<RDFDatatype> counter = new Counter<>();
//        for (Map.Entry<TinyNode, Integer> e : objects.entrySet()) {
//            //TODO: hirearchia typow, por. http://www.w3.org/TR/xmlschema-2/#built-in-datatypes
//            //zobacz tez RDFDatatype.normalizeSubType
//            if (!e.getKey().isLiteral()) {
//                continue;
//            }
//            counter.inc(e.getKey().asLiteral().getDatatype(), e.getValue());
//        }
//        List<DatatypePropertyPattern> result = new ArrayList<>();
//        for (Map.Entry<RDFDatatype, Integer> e : counter.entrySet()) {
//            double quality = ((double) e.getValue()) / objects.getSum();
//            if (quality >= cfg.getMinSupport()) {
//                result.add(new DatatypePropertyPattern(p, e.getKey(), quality));
//            }
//        }
//	//TODO: tu w zasadzie nalezaloby patrzec co jezeli jest duzo roznych typow, z ktorych zaden nie ma minsupport, ale moznaby przynajmniej robic literal
//        patterns.addAll(result);
//    }
//rdf:PlainLiteral
//rdf:XMLLiteral
//rdfs:Literal
//owl:real
//owl:rational
//xsd:decimal
//xsd:integer
//xsd:nonNegativeInteger
//xsd:string
//xsd:normalizedString
//xsd:token
//xsd:Name
//xsd:NCName
//xsd:NMTOKEN
//xsd:hexBinary
//xsd:base64Binary
//xsd:anyURI
//xsd:dateTime
//xsd:dateTimeStamp
    private static final String RDF_PLAINLITERAL = "http://www.w3.org/1999/02/22-rdf-syntax-ns#PlainLiteral";
    private static final String RDF_XMLLITERAL = "http://www.w3.org/1999/02/22-rdf-syntax-ns#XMLLiteral";
    private static final String RDFS_LITERAL = "http://www.w3.org/2000/01/rdf-schema#Literal";
    private static final String OWL_REAL = "http://www.w3.org/2002/07/owl#real";
    private static final String OWL_RATIONAL = "http://www.w3.org/2002/07/owl#rational";
    private static final String XSD_DECIMAL = "http://www.w3.org/2001/XMLSchema#decimal";
    private static final String XSD_INTEGER = "http://www.w3.org/2001/XMLSchema#integer";
    private static final String XSD_NONNEGATIVEINTEGER = "http://www.w3.org/2001/XMLSchema#nonNegativeInteger";
    private static final String XSD_STRING = "http://www.w3.org/2001/XMLSchema#string";
    private static final String XSD_NORMALIZEDSTRING = "http://www.w3.org/2001/XMLSchema#normalizedString";
    private static final String XSD_TOKEN = "http://www.w3.org/2001/XMLSchema#token";
    private static final String XSD_NAME = "http://www.w3.org/2001/XMLSchema#Name";
    private static final String XSD_NCNAME = "http://www.w3.org/2001/XMLSchema#NCName";
    private static final String XSD_NMTOKEN = "http://www.w3.org/2001/XMLSchema#NMTOKEN";
    private static final String XSD_HEXBINARY = "http://www.w3.org/2001/XMLSchema#hexBinary";
    private static final String XSD_BASE64BINARY = "http://www.w3.org/2001/XMLSchema#base64Binary";
    private static final String XSD_ANYURI = "http://www.w3.org/2001/XMLSchema#anyURI";
    private static final String XSD_DATETIME = "http://www.w3.org/2001/XMLSchema#dateTime";
    private static final String XSD_DATETIMESTAMP = "http://www.w3.org/2001/XMLSchema#dateTimeStamp";

    public static final List<String> ALLOWED_IN_OWL2EL = Arrays.asList(
            RDF_PLAINLITERAL,
            RDF_XMLLITERAL,
            RDFS_LITERAL,
            OWL_REAL,
            OWL_RATIONAL,
            XSD_DECIMAL,
            XSD_INTEGER,
            XSD_NONNEGATIVEINTEGER,
            XSD_STRING,
            XSD_NORMALIZEDSTRING,
            XSD_TOKEN,
            XSD_NAME,
            XSD_NCNAME,
            XSD_NMTOKEN,
            XSD_HEXBINARY,
            XSD_BASE64BINARY,
            XSD_ANYURI,
            XSD_DATETIME,
            XSD_DATETIMESTAMP
    );

    private List<String> realNumbers(TinyLiteral l) {
        //The owl:real datatype does not directly provide any lexical forms.
        try {
            Double.parseDouble(l.getLexicalForm());
            return Arrays.asList(OWL_REAL, XSD_DECIMAL);
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    private List<String> integers(TinyLiteral l) {
        try {
            int i = Integer.parseInt(l.getLexicalForm());
            if (i >= 0) {
                return Arrays.asList(XSD_INTEGER, XSD_NONNEGATIVEINTEGER);
            } else {
                return Arrays.asList(XSD_INTEGER);
            }
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
    }

    private List<String> uri(TinyLiteral l) {
        try {
            if (l.getLexicalForm().contains(":")) {
                //well, RFC3986 requires URI to contain a scheme and a colon after it, but apparently URI constructor doesn't bother and happily parses 395.3 as a valid URI
                new URI(l.getLexicalForm());    //ignore the result
                return Arrays.asList(XSD_ANYURI);
            }
        } catch (Exception e) {
        }
        return Collections.EMPTY_LIST;
    }

    //'-'? yyyy '-' mm '-' dd 'T' hh ':' mm ':' ss ('.' s+)? (zzzzzz)?
    //(('+' | '-') hh ':' mm) | 'Z'
    public static final Pattern DATETIME_PATTERN = Pattern.compile("^-?[0-9]{4,}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?(((\\+|-)[0-9]{2}:[0-9]{2})|Z)?$");

    private List<String> dateTime(TinyLiteral l) {
        if (DATETIME_PATTERN.matcher(l.getLexicalForm()).matches()) {
            return Arrays.asList(XSD_DATETIME);
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    //arbitrary ordering to make results more user-friendly
    private final List<String> ORDER = Arrays.asList(XSD_DATETIME, XSD_NONNEGATIVEINTEGER, XSD_DECIMAL, OWL_REAL);

    private Set<String> findDatatypes(TinyLiteral l) {
        String declaredDatatype = l.getDatatypeURI();
        if (declaredDatatype != null && ALLOWED_IN_OWL2EL.contains(declaredDatatype)) {
            return new HashSet<>(Arrays.asList(declaredDatatype));
        }
        Set<String> result = new HashSet<>();
        result.addAll(realNumbers(l));
        result.addAll(integers(l));
        result.addAll(uri(l));
        result.addAll(dateTime(l));
        if (result.isEmpty()) {
            if (l.getLanguage() != null && !l.getLanguage().isEmpty()) {
                //seems there's some inconsistency in documentation:
                // https://www.w3.org/TR/rdf-plain-literal/ allows for a language tag in rdf:PlainLiteral,
                //but https://www.w3.org/TR/rdf11-concepts/ requires the type to be http://www.w3.org/1999/02/22-rdf-syntax-ns#langString which is not supported in OWL2 EL
                //and https://www.w3.org/TR/rdf-schema/#ch_literal offers rdf:langString which also allows for language tags
                result.add(RDF_PLAINLITERAL);
            } else {
                result.add(XSD_STRING);
            }
        }
        return result;
    }

    @Override
    public List<PartialPattern> find(Subgraph graph, TinyResource p, Weight w) {
        Map<String, Set<TinyNode>> datatypes = new HashMap<>();
        for (Map.Entry<TinyNode, List<Triple>> i : graph.getOSforP(p).entrySet()) {
            TinyNode o = i.getKey();
            List<Triple> triples = i.getValue();
            if (!o.isLiteral()) {
                continue;
            }
            Set<String> candidates = findDatatypes(o.asLiteral());
            for (String dt : candidates) {
                double val = 0;
                Set<TinyNode> s = datatypes.get(dt);
                if (s == null) {
                    s = new HashSet<>();
                    datatypes.put(dt, s);
                }
                for (Triple t : triples) {
                    s.add(t.getS());
                }
            }
        }
        List<PartialPattern> result = new ArrayList<>();
        PartialPattern best = null;
        int bestIndex = ORDER.size() + 1;
        for (Map.Entry<String, Set<TinyNode>> i : datatypes.entrySet()) {
            double s = w.support(i.getValue());
            if (s >= cfg.getMinSupport()) {
                PartialPattern pat = new DatatypePropertyPattern(p, i.getKey(), s, i.getValue());
                result.add(pat);
                int index = ORDER.indexOf(i.getKey());
                if (index < 0) {
                    continue;
                }
                if (index < bestIndex) {
                    best = pat;
                    bestIndex = index;
                }
            }
        }
        if (best != null) {
            return Arrays.asList(best);
        } else {
            return result;
        }
    }

}
