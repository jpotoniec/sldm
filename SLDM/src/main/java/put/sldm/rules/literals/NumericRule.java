package put.sldm.rules.literals;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import put.sldm.config.Config;
import put.sldm.tiny.TinyLiteral;

public class NumericRule extends ComparableRule {

    public NumericRule(Config cfg) {
        super(cfg);
    }

    @Override
    protected Comparable cast(TinyLiteral l) {
        try {
            return l.getDouble();
        } catch (Exception ex) {
            //l is not a number, it's all right
            return null;
        }
    }

    @Override
    protected RDFDatatype getDatatype() {
        return XSDDatatype.XSDdecimal;
    }

}
