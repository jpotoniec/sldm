package put.sldm.rules.literals;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import put.sldm.Counter;
import put.sldm.Weight;
import put.sldm.config.Config;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.patterns.partial.RangePattern;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.rules.AbstractRule;
import put.sldm.rules.DatatypeRule;
import put.sldm.tiny.TinyLiteral;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public abstract class ComparableRule extends AbstractRule {

    private static final Logger logger = Logger.getLogger(ComparableRule.class);

    public ComparableRule(Config cfg) {
        super(cfg);
    }

    /**
     * @return Value of a literal l if it makes sense and null if it does not
     */
    protected abstract Comparable cast(TinyLiteral l);

    protected abstract RDFDatatype getDatatype();

    private TinyLiteral ensureTypedLiteral(TinyLiteral l) {
        if (l.getDatatype() == null || !DatatypeRule.ALLOWED_IN_OWL2EL.contains(l.getDatatypeURI())) {
            l.setDatatype(getDatatype());
        }
        return l;
    }

    @Override
    public List<PartialPattern> find(Subgraph graph, TinyResource p, Weight w) {
//    public void find(Counter<TinyNode> objects, TinyResource p, List<PartialPattern> patterns, Counter<TinyNode> I, Subgraph g) {
        Comparable min = null;
        TinyLiteral minLit = null;
        Comparable max = null;
        TinyLiteral maxLit = null;
        int n = 0;
        Set<TinyNode> proofSet = new HashSet<>();
        for (Map.Entry<TinyNode, List<Triple>> i : graph.getOSforP(p).entrySet()) {
            try {
                if (!i.getKey().isLiteral()) {
                    continue;
                }
                TinyLiteral l = i.getKey().asLiteral();
                logger.trace(String.format("Considering '%s'", l));
                Comparable val = cast(l);
                if (val == null) {
                    continue;
                }
                if (min == null || min.compareTo(val) > 0) {
                    min = val;
                    minLit = l;
                    logger.trace(String.format("New minimal value is %s (value is %s)", minLit, min));
                }
                if (max == null || max.compareTo(val) < 0) {
                    max = val;
                    maxLit = l;
                    logger.trace(String.format("New maximal value is %s (value is %s)", maxLit, max));
                }
                for (Triple t : i.getValue()) {
                    proofSet.add(t.getS());
                }
            } catch (Exception ex) {
                //wyjatki sa raczej niespodziewane, wiec nie polykamy ich milczaco
                logger.error(i.getKey(), ex);
            }
        }
        double quality = w.support(proofSet);
        if (quality >= cfg.getMinSupport()) {
            assert minLit != null;
            assert maxLit != null;
            List<PartialPattern> patterns = new ArrayList<>();
            patterns.add(new RangePattern(p, RangePattern.Type.MIN, ensureTypedLiteral(minLit), quality, proofSet));
            patterns.add(new RangePattern(p, RangePattern.Type.MAX, ensureTypedLiteral(maxLit), quality, proofSet));
            return patterns;
        } else {
            return Collections.EMPTY_LIST;
        }
    }
}
