package put.sldm.rules;

import put.sldm.config.Config;

public abstract class AbstractRule implements Rule {

    protected Config cfg;

    public AbstractRule(Config cfg) {
        this.cfg = cfg;
    }

}
