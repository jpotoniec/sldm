package put.sldm.rules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import put.sldm.Weight;
import put.sldm.config.Config;
import put.sldm.patterns.partial.ConcretePattern;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class FillerRule extends AbstractRule {

    public FillerRule(Config cfg) {
        super(cfg);
    }

    @Override
    public List<PartialPattern> find(Subgraph graph, TinyResource p, Weight w) {
        List<PartialPattern> result = new ArrayList<>();
        Map<TinyNode, List<Triple>> os = graph.getOSforP(p);
        for (Map.Entry<TinyNode, List<Triple>> i : os.entrySet()) {
            TinyNode o = i.getKey();
            Set<TinyNode> proofSet = new HashSet<>();
            for (Triple t : i.getValue()) {
                proofSet.add(t.getS());
            }
            double s = w.support(proofSet);
            if (s >= cfg.getMinSupport()) {
                result.add(new ConcretePattern(p, o, s, proofSet));
            }
        }
        return result;
    }

}
