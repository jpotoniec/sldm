package put.sldm.rules;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import put.sldm.Weight;
import put.sldm.config.Config;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.patterns.partial.SelfPattern;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class SelfRule extends AbstractRule {

    public SelfRule(Config cfg) {
        super(cfg);
    }

    @Override
    public List<PartialPattern> find(Subgraph graph, TinyResource p, Weight w) {
        List<PartialPattern> result = new ArrayList<>();
        Map<TinyNode, List<Triple>> os = graph.getOSforP(p);
        double s = 0;
        Set<TinyNode> proofSet = new HashSet<>();
        for (Map.Entry<TinyNode, List<Triple>> i : os.entrySet()) {
            TinyNode o = i.getKey();
            if (o.isResource()) {
                boolean found = false;
                for (Triple t : i.getValue()) {
                    if (t.getS().equals(o)) {
                        found = true;
                        break;
                    }
                }
                if (found) {
                    s += w.support(o);
                    proofSet.add(o);
                }
            }
        }
        if (s >= cfg.getMinSupport()) {
            result.add(new SelfPattern(p, s, proofSet));
        }
        return result;
    }

}
