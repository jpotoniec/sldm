package put.sldm.rules;

import java.util.List;
import put.sldm.Weight;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.tiny.TinyResource;

public interface Rule {
    
    public List<PartialPattern> find(Subgraph graph, TinyResource p, Weight w);
}
