package put.sldm.tiny;

import com.hp.hpl.jena.datatypes.DatatypeFormatException;
import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.Objects;
import org.apache.log4j.Logger;

public class TinyLiteral extends TinyNode {

    private final static int MAX_LEN = 100;
    private final static Logger logger = Logger.getLogger(TinyLiteral.class);

    private RDFDatatype datatype;
    private final Object value;
    private final String lexicalForm;
    private final String language;

    public TinyLiteral(Literal original) {
        this.datatype = original.getDatatype();
        Object tmp = null;
        try {
            tmp = original.getValue();
            if (tmp instanceof String && ((String) tmp).length() > MAX_LEN) {
                tmp = ((String) tmp).substring(0, MAX_LEN);
            }
        } catch (DatatypeFormatException ex) {
            if (!ex.getMessage().contains("http://www.w3.org/2001/XMLSchema#gYear")) {
                logger.warn(String.format("Error while parsing %s", original), ex);
            }
        }
        this.value = tmp;
        if (value instanceof String) {
            this.lexicalForm = null;
        } else {
            this.lexicalForm = original.getLexicalForm();
        }
        this.language = original.getLanguage();
    }

    public String getLexicalForm() {
        if (value != null && value instanceof String) {
            return (String) value;
        } else {
            return lexicalForm;
        }
    }

    @Override
    public TinyLiteral asLiteral() {
        return this;
    }

    public RDFDatatype getDatatype() {
        return datatype;
    }

    public void setDatatype(RDFDatatype datatype) {
        this.datatype = datatype;
    }

    public String getDatatypeURI() {
        if (datatype != null) {
            return datatype.getURI();
        } else {
            return null;
        }
    }

    public String getLanguage() {
        return language;
    }

    public Object getValue() {
        return value;
    }

    public double getDouble() {
        return Double.parseDouble(lexicalForm);
    }

    public RDFNode toJena(Model model) {
        if (datatype != null) {
            return model.createTypedLiteral(getLexicalForm(), datatype);
        }
        return model.createLiteral(getLexicalForm(), language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datatype, language, getLexicalForm(), value);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TinyLiteral other = (TinyLiteral) obj;
        if (!Objects.equals(this.datatype, other.datatype)) {
            return false;
        }
        if (!Objects.equals(this.value, other.value)) {
            return false;
        }
        if (!Objects.equals(this.getLexicalForm(), other.getLexicalForm())) {
            return false;
        }
        if (!Objects.equals(this.language, other.language)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        if (datatype != null) {
            return String.format("\"%s\"^^<%s>", getLexicalForm(), datatype.getURI());
        }
        if (language != null && !language.isEmpty()) {
            return String.format("\"%s\"@\"%s\"", getLexicalForm(), language);
        }
        return String.format("\"%s\"", getLexicalForm());
    }

}
