package put.sldm.tiny;

import com.hp.hpl.jena.rdf.model.RDFNode;

public abstract class TinyNode {

    public static TinyNode produce(RDFNode original) {
        if (original.isResource()) {
            return new TinyResource(original.asResource());
        }
        if (original.isLiteral()) {
            return new TinyLiteral(original.asLiteral());
        }
        throw new IllegalArgumentException(String.format("Uh-huh, what am I supposed to do with '%s'?", original));
    }

    public TinyResource asResource() {
        return null;
    }

    public boolean isResource() {
        return asResource() != null;
    }

    public boolean isURIResource() {
        return isResource();
    }

    public TinyLiteral asLiteral() {
        return null;
    }

    public boolean isLiteral() {
        return asLiteral() != null;
    }
}
