package put.sldm.tiny;

import com.hp.hpl.jena.rdf.model.Resource;
import java.util.Objects;

public class TinyResource extends TinyNode {

    private final String uri;

    public TinyResource(String uri) {
        this.uri = uri.intern();
    }

    public TinyResource(Resource original) {
        this(original.getURI());
    }

    public String getURI() {
        return uri;
    }

    @Override
    public TinyResource asResource() {
        return this;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TinyResource other = (TinyResource) obj;
        if (!Objects.equals(this.uri, other.uri)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return uri;
    }

}
