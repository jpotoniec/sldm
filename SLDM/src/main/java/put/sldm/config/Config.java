package put.sldm.config;

import put.sldm.sampling.SamplingStrategy;
import put.sldm.sampling.SamplingStrategyFactory;

import java.util.List;
import java.util.regex.Pattern;

public interface Config {

    String getEndpoint();

    List<Pattern> getIgnoredProperties();

    String getInitialQuery();

    int getMaxLevel();

    int getMaxOpenLevel();

    int getMaxValuesSize();

    double getMinFunctionalSupport();

    double getMinSupport();

    Long getRandomSeed();

    boolean getRemoteValidation();

    int getSampleSize();

    boolean isSamplingEnabled();

    SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy();

}
