package put.sldm;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class Weight {

    private final Map<TinyNode, Double> data = new HashMap<>();
    private double sum;

    public Weight(Collection<? extends TinyNode> items) {
        double p = 1.0 / items.size();
        for (TinyNode item : items) {
            data.put(item, p);
        }
        sum = 1.0;
    }

    public Weight(Weight w, Subgraph g, TinyResource p) {
        Map<TinyNode, List<Triple>> os = g.getOSforP(p);
        Counter<TinyResource> denominator = new Counter<>();
        for (Map.Entry<TinyNode, List<Triple>> i : os.entrySet()) {
            TinyNode o = i.getKey();
            for (Triple t : i.getValue()) {
                TinyResource s = t.getS();
                denominator.inc(s);
            }
        }
        for (Map.Entry<TinyNode, List<Triple>> i : os.entrySet()) {
            TinyNode o = i.getKey();
            double val = 0;
            for (Triple t : i.getValue()) {
                TinyResource s = t.getS();
                assert denominator.get(s) >= 1;
                val += w.support(s) / denominator.get(s);
            }
            data.put(o, val);
            sum += val;
        }
        assert sum <= w.getSum() + 0.0001;
    }

    public double support(Iterable<? extends TinyNode> items) {
        double result = 0;
        for (TinyNode item : items) {
            result += support(item);
        }
        return result;
    }

    public double support(TinyNode item) {
        if (data.containsKey(item)) {
            return data.get(item);
        } else {
            return 0;
        }
    }

    public double supportForSubjects(Iterable<Triple> items) {
        double result = 0;
        for (Triple item : items) {
            result += support(item.getS());
        }
        return result;
    }

    public Set<TinyNode> getBase() {
        return data.keySet();
    }

    public double getSum() {
        return sum;
    }

}
