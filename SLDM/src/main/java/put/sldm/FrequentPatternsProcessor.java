package put.sldm;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import put.sldm.patterns.FullPattern;

public class FrequentPatternsProcessor implements SLDMListener {

    private GregorianCalendar begin = null, end = null;
    private int baseSize;
    private final List<FullPattern> result = new ArrayList<>();

    @Override
    public void miningStarted() {
        begin = new GregorianCalendar();
    }

    @Override
    public void baseDetermined(int size) {
        baseSize = size;
    }

    @Override
    public void miningFinished() {
        end = new GregorianCalendar();
    }

    @Override
    public void patternMined(FullPattern p) {
        result.add(p);
    }

    public FrequentPatterns deliver() {
        return new FrequentPatterns(result, baseSize, begin, end);
    }

    @Override
    public boolean shouldStop() {
        return false;
    }

    @Override
    public void stateChanged(State current) {
    }
}
