package put.sldm;

import java.util.HashMap;

public class Counter<K> extends HashMap<K, Integer> {

    private int sum;

    public Counter() {
        super();
    }

    public Counter(Iterable<? extends K> objects) {
        this();
        for (K object : objects) {
            inc(object);
        }
    }

    public void inc(K key, int n) {
        int val = n;
        if (this.containsKey(key)) {
            val += this.get(key);
        }
        sum += n;
        this.put(key, val);
    }

    public void inc(K key) {
        inc(key, 1);
    }

    public int getSum() {
        return sum;
    }

}
