package put.sldm.patterns.partial;

import java.util.Objects;
import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDataRange;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class DatatypePropertyPattern extends PartialPattern {

    private final String datatype;

    public DatatypePropertyPattern(TinyResource property, String datatype, double quality, Set<TinyNode> proofSet) {
        super(property, quality, proofSet);
        this.datatype = datatype;
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String nv = gen.next();
        String text;
        if (datatype != null) {
            text = String.format("%1$s %2$s %3$s.\nfilter(datatype(%3$s)=<%4$s>)\n", var, Utils.toString(getProperty()), nv, datatype);
        } else {
            text = String.format("%1$s %2$s %3$s.\nfilter(isLiteral(%3$s))\n", var, Utils.toString(getProperty()), nv);
        }
        return new SPARQLPatternPart(text, nv);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        OWLDataPropertyExpression p = m.getOWLDataProperty(IRI.create(getProperty().getURI()));
        OWLDataRange r = m.getOWLDatatype(IRI.create(datatype));
        return m.getOWLDataSomeValuesFrom(p, r);
    }

    @Override
    public String toString() {
        return String.format("(%s range %s)", getProperty(), datatype);
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof DatatypePropertyPattern)) {
            return false;
        }
        DatatypePropertyPattern other = (DatatypePropertyPattern) obj;
        return Objects.equals(this.datatype, other.datatype);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        return 97 * hash + Objects.hash(this.datatype);
    }

}
