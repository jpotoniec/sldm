package put.sldm.patterns.partial;

import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class FunctionalPattern extends PartialPattern {

    private final List<TinyNode> objects;

    public FunctionalPattern(TinyResource p, List<TinyNode> objects, double quality, Set<TinyNode> proofSet) {
        super(p, quality, proofSet);
        this.objects = objects;
        if (objects.isEmpty()) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return String.format("(%s=%s)", getProperty(), StringUtils.join(objects, " "));
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String result = "";
        for (TinyNode o : objects) {
            if (!result.isEmpty()) {
                result += "\nUNION\n";
            }
            result += String.format("{%s %s %s}", var, Utils.toString(getProperty()), Utils.toString(o));
        }
        return new SPARQLPatternPart("{\n" + result + "\n}", null);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    public List<TinyNode> getObjects() {
        return objects;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof FunctionalPattern)) {
            return false;
        }
        FunctionalPattern other = (FunctionalPattern) obj;
        return CollectionUtils.isEqualCollection(objects, other.objects);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 97 * hash + Objects.hashCode(this.objects);
        return hash;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        if (subclass != null) {
            throw new IllegalArgumentException();
        }
        Set<OWLLiteral> literals = new HashSet<>();
        Set<OWLIndividual> individuals = new HashSet<>();
        for (TinyNode o : objects) {
            if (o.isLiteral()) {
                literals.add(Utils.convert(m, o.asLiteral()));
            } else {
                individuals.add(m.getOWLNamedIndividual(IRI.create(o.asResource().getURI())));
            }
        }
        OWLClassExpression a = null, b = null;
        if (!literals.isEmpty()) {
            OWLDataProperty p = m.getOWLDataProperty(IRI.create(getProperty().getURI()));
            a = m.getOWLDataSomeValuesFrom(p, m.getOWLDataOneOf(literals));
        }
        if (!individuals.isEmpty()) {
            OWLObjectProperty p = m.getOWLObjectProperty(IRI.create(getProperty().getURI()));
            a = m.getOWLObjectSomeValuesFrom(p, m.getOWLObjectOneOf(individuals));
        }
        assert a != null || b != null;
        if (a != null) {
            if (b != null) {
                return m.getOWLObjectUnionOf(a, b);
            } else {
                return a;
            }
        } else {
            return b;
        }
    }

}
