package put.sldm.patterns.partial;

import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import put.sldm.Utils;
import put.sldm.Weight;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class ObjectsPattern extends PartialPattern {

    private final Weight objects;

    public ObjectsPattern(TinyResource property, Weight objects, double quality, Set<? extends TinyNode> proofSet) {
        super(property, quality, proofSet);
        this.objects = objects;
    }

    public Weight getObjects() {
        return objects;
    }

    @Override
    public String toString() {
        return String.format("(%s ?)", getProperty());
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String p = Utils.toString(getProperty());
        String nv = gen.next();
        return new SPARQLPatternPart(String.format("%s %s %s.\n", var, p, nv), nv);
    }

    @Override
    public boolean isOpen() {
        return true;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        if (subclass == null) {
            subclass = m.getOWLThing();
        }
        OWLObjectPropertyExpression p = m.getOWLObjectProperty(IRI.create(getProperty().getURI()));
        return m.getOWLObjectSomeValuesFrom(p, subclass);
    }
}
