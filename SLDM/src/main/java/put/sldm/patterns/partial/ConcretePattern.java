package put.sldm.patterns.partial;

import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class ConcretePattern extends PartialPattern {

    private final TinyNode object;

    public ConcretePattern(TinyResource property, TinyNode object, double quality, Set<TinyNode> proofSet) {
        super(property, quality, proofSet);
        this.object = object;
    }

    public TinyNode getObject() {
        return object;
    }

    @Override
    public String toString() {
        return String.format("(%s %s)", getProperty(), object);
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String p = Utils.toString(getProperty());
        return new SPARQLPatternPart(String.format("%s %s %s.\n", var, p, Utils.toString(getObject())), null);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        if (subclass != null) {
            throw new IllegalArgumentException();
        }
        if (RDF_TYPE.equals(getProperty().getURI())) {
            return m.getOWLClass(IRI.create(object.asResource().getURI()));
        } else {
            if (object.isLiteral()) {
                OWLDataPropertyExpression p = m.getOWLDataProperty(IRI.create(getProperty().getURI()));
                OWLLiteral l = Utils.convert(m, object.asLiteral());
                return m.getOWLDataHasValue(p, l);
            } else {
                OWLObjectPropertyExpression p = m.getOWLObjectProperty(IRI.create(getProperty().getURI()));
                OWLIndividual i = m.getOWLNamedIndividual(IRI.create(object.asResource().getURI()));
                return m.getOWLObjectHasValue(p, i);
            }
        }
    }

}
