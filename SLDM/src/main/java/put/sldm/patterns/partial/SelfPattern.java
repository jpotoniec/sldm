package put.sldm.patterns.partial;

import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class SelfPattern extends PartialPattern {

    public SelfPattern(TinyResource property, double quality, Set<TinyNode> proofSet) {
        super(property, quality, proofSet);
    }

    @Override
    public String toString() {
        return String.format("(%s SELF)", getProperty());
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String p = Utils.toString(getProperty());
        return new SPARQLPatternPart(String.format("%s %s %s.\n", var, p, var), null);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        if (subclass != null) {
            throw new IllegalArgumentException();
        }
        OWLObjectPropertyExpression p = m.getOWLObjectProperty(IRI.create(getProperty().getURI()));
        return m.getOWLObjectHasSelf(p);
    }

}
