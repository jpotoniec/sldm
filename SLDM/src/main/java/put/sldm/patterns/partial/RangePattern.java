package put.sldm.patterns.partial;

import java.util.Objects;
import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.vocab.OWLFacet;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyLiteral;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class RangePattern extends PartialPattern {

    public enum Type {

        MIN, MAX
    };
    private final Type type;
    private final TinyLiteral value;

    public RangePattern(TinyResource property, Type type, TinyLiteral value, double quality, Set<TinyNode> proofSet) {
        super(property, quality, proofSet);
        this.type = type;
        this.value = value;
    }

    public TinyLiteral getValue() {
        return value;
    }

    private static String toString(Type type) {
        if (type == null) {
            throw new NullPointerException();
        }
        switch (type) {
            case MIN:
                return ">=";
            case MAX:
                return "<=";
            default:
                throw new IllegalArgumentException();
        }
    }

    @Override
    public String toString() {
        return String.format("(%s %s %s)", getProperty(), toString(type), value);
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String p = Utils.toString(getProperty());
        String sval = Utils.toString(value);
        String valType = value.getDatatypeURI();
        String nv = gen.next();
        String text = String.format("%1$s %2$s %3$s.\nfilter(<%6$s>(%3$s) %5$s %4$s)\n", var, p, nv, sval, toString(type), valType);
        return new SPARQLPatternPart(text, nv);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof RangePattern)) {
            return false;
        }
        RangePattern other = (RangePattern) obj;
        return this.type == other.type && this.value.equals(other.value);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 79 * hash + Objects.hashCode(this.type);
        hash = 79 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        /* See:
         http://www.w3.org/TR/owl2-syntax/#Data_Ranges
         https://github.com/ghillairet/emf4sw/blob/master/bundles/com.emf4sw.owl.jena/src/com/emf4sw/owl/resource/impl/DataRangeFactory.java
         http://www.w3.org/TR/2012/REC-owl2-mapping-to-rdf-20121211/#Translation_of_Axioms_without_Annotations
         http://www.w3schools.com/schema/schema_facets.asp
         */
        if (subclass != null) {
            throw new IllegalArgumentException();
        }
        OWLDatatype datatype = m.getOWLDatatype(IRI.create(value.getDatatypeURI()));
        OWLFacet facet;
        OWLLiteral l = Utils.convert(m, value);
        switch (type) {
            case MAX:
                facet = OWLFacet.MAX_INCLUSIVE;
                break;
            case MIN:
                facet = OWLFacet.MIN_INCLUSIVE;
                break;
            default:
                throw new IllegalStateException();
        }
        OWLDataPropertyExpression p = m.getOWLDataProperty(IRI.create(getProperty().getURI()));
        return m.getOWLDataSomeValuesFrom(p, m.getOWLDatatypeRestriction(datatype, facet, l));
    }

}
