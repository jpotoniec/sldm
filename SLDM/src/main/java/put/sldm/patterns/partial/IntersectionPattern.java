package put.sldm.patterns.partial;

import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;

public class IntersectionPattern extends PartialPattern {

    private final Set<PartialPattern> items;

    public IntersectionPattern(Set<PartialPattern> items, double quality, Set<? extends TinyNode> proofSet) {
        super(null, quality, proofSet);
        this.items = items;
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String text = "";
        for (PartialPattern i : items) {
            text += i.toSPARQL(var, gen).getText();
        }
        return new SPARQLPatternPart(text, null);
    }

    @Override
    public boolean isOpen() {
        return false;
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory factory, OWLClassExpression subclass) {
        assert subclass == null;
        Set<OWLClassExpression> operands = new HashSet<>();
        for (PartialPattern i : items) {
            operands.add(i.createClass(factory, null));
        }
        return factory.getOWLObjectIntersectionOf(operands);
    }

    @Override
    public String toString() {
        return StringUtils.join(items, " AND ");
    }

}
