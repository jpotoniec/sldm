package put.sldm.patterns.partial;

import java.util.Objects;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public abstract class PartialPattern {

    public static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    private final TinyResource property;
    private final double quality;
    private final Set<? extends TinyNode> proofSet;

    /**
     *
     * @param property
     * @param quality In range [0,1] to estimate quality of a full pattern by
     * multiplication
     */
    public PartialPattern(TinyResource property, double quality, Set<? extends TinyNode> proofSet) {
        this.property = property;
        this.quality = quality;
        this.proofSet = proofSet;
    }

    public TinyResource getProperty() {
        return property;
    }

    public abstract SPARQLPatternPart toSPARQL(String var, VariableGenerator gen);

    public abstract boolean isOpen();

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof PartialPattern)) {
            return false;
        }
        //TODO: zastanowic sie czy equals i hashCode powinny uwzgledniac quality czy nie
        return property.equals(((PartialPattern) obj).property);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.property);
    }

    public abstract OWLClassExpression createClass(OWLDataFactory factory, OWLClassExpression subclass);

    public double getQuality() {
        return quality;
    }

    public Set<? extends TinyNode> getProofSet() {
        return proofSet;
    }

}
