package put.sldm.patterns.partial;

import java.util.Set;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import put.sldm.Utils;
import put.sldm.patterns.SPARQLPatternPart;
import put.sldm.patterns.VariableGenerator;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class OpenFunctionalPattern extends PartialPattern {

    private static final IRI OWL_CLASS = IRI.create("http://www.w3.org/2002/07/owl#Class");

    public OpenFunctionalPattern(TinyResource property, double quality, Set<TinyNode> proofSet) {
        super(property, quality, proofSet);
    }

    @Override
    public SPARQLPatternPart toSPARQL(String var, VariableGenerator gen) {
        String p = Utils.toString(getProperty());
        String v1 = gen.next();
        String v2 = gen.next();
        String text = String.format("%1$s %2$s %3$s.\nfilter not exists\n{\n%1$s %2$s %4$s.\nfilter(!sameTerm(%3$s, %4$s))\n}\n", var, p, v1, v2);
        return new SPARQLPatternPart(text, v1);
    }

    @Override
    public boolean isOpen() {
        return false;   //nie moze byc true, bo QRestriction jest niewspierane nawet w OWL Full 
    }

    @Override
    public String toString() {
        return String.format("(%s is functional)", getProperty());
    }

    @Override
    public OWLClassExpression createClass(OWLDataFactory m, OWLClassExpression subclass) {
        //TODO: a co z DataProperty? jak je w ogóle rozróżnić?!
        OWLObjectPropertyExpression p = m.getOWLObjectProperty(IRI.create(getProperty().getURI()));
        if (RDF_TYPE.equals(getProperty().getURI())) {
            return m.getOWLObjectExactCardinality(1, p, m.getOWLClass(OWL_CLASS));
        }
        return m.getOWLObjectExactCardinality(1, p);
    }

}
