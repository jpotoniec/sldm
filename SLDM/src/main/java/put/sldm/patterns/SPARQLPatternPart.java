package put.sldm.patterns;

public class SPARQLPatternPart {

    private final String text;
    private final String objectVariable;

    public SPARQLPatternPart(String text, String objectVariable) {
        this.text = text;
        this.objectVariable = objectVariable;
    }

    public String getText() {
        return text;
    }

    public String getObjectVariable() {
        return objectVariable;
    }

    public boolean isExtendable() {
        return objectVariable != null;
    }
}
