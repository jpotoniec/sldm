package put.sldm.patterns;

public class DefaultVariableGenerator implements VariableGenerator {

    private int n = 1;

    @Override
    public String next() {
        return String.format("?var%d", n++);
    }

}
