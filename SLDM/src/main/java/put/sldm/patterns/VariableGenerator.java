package put.sldm.patterns;

public interface VariableGenerator {

    public String next();
}
