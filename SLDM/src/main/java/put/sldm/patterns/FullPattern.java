package put.sldm.patterns;

import com.hp.hpl.jena.ontology.OntModel;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.List;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import put.sldm.patterns.partial.PartialPattern;

public class FullPattern {

    private List<PartialPattern> partialPatterns;
    private int quality = 0;

    public FullPattern(Collection<PartialPattern> path) {
        partialPatterns = new ArrayList<>(path);
        partialPatterns = Collections.unmodifiableList(partialPatterns);
    }

    public FullPattern(Collection<PartialPattern> path, PartialPattern last) {
        partialPatterns = new ArrayList<>(path);
        partialPatterns.add(last);
        partialPatterns = Collections.unmodifiableList(partialPatterns);
    }

    public List<PartialPattern> getPartialPatterns() {
        return partialPatterns;
    }

    public String toSPARQL(String subject, VariableGenerator gen) {
        String result = "";
        for (PartialPattern p : partialPatterns) {
            SPARQLPatternPart x = p.toSPARQL(subject, gen);
            result += x.getText();
            if (!x.isExtendable()) {
                //TODO: fajnie by bylo tutaj sprawdzac czy jestesmy na koncu kolekcji i jezeli nie, to moze informowac o tym
                break;
            } else {
                subject = x.getObjectVariable();
            }
        }
        return result;
    }

    @Override
    public String toString() {
        String result = "";
        for (PartialPattern p : partialPatterns) {
            result += p.toString();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof FullPattern)) {
            return false;
        }
        FullPattern other = (FullPattern) obj;
        return Arrays.equals(this.partialPatterns.toArray(), other.partialPatterns.toArray());
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(partialPatterns.toArray());
    }

    public OWLClassExpression createClass(OWLDataFactory m) {
        OWLClassExpression prev = null;
        for (int i = partialPatterns.size() - 1; i >= 0; i--) {
            PartialPattern p = partialPatterns.get(i);
            prev = p.createClass(m, prev);
        }
        return prev;
    }

    public void setQuality(int quality) {
        this.quality = quality;
    }

    public int getQuality() {
        return quality;
    }

    public double estimateQuality() {
//        double result = 1;
//        for (PartialPattern p : partialPatterns) {
//            result *= p.getQuality();
//        }
//        return result;
        return partialPatterns.get(partialPatterns.size() - 1).getQuality();
    }
}
