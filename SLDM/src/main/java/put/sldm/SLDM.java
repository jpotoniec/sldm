package put.sldm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import put.sldm.config.Config;
import put.sldm.patterns.DefaultVariableGenerator;
import put.sldm.patterns.FullPattern;
import put.sldm.patterns.partial.IntersectionPattern;
import put.sldm.patterns.partial.ObjectsPattern;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.rdfgraph.Graph;
import put.sldm.rdfgraph.JenaGraph;
import put.sldm.rdfgraph.Subgraph;
import put.sldm.rdfgraph.Triple;
import put.sldm.rules.DatatypeRule;
import put.sldm.rules.FillerRule;
import put.sldm.rules.Rule;
import put.sldm.rules.SelfRule;
import put.sldm.rules.literals.NumericRule;
import put.sldm.sampling.SamplingStrategyFactory;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class SLDM {

    private final static Logger logger = Logger.getLogger(SLDM.class);

    private final Config cfg;
    private final SPARQL sparql;
    private List<TinyResource> base;
    private Graph fullGraph;
    private final List<Rule> rules;
    private final Random rnd;
    private ExecutorService executorService;

    private boolean isIgnored(TinyResource r) {
        if (r.isURIResource()) {
            for (Pattern p : cfg.getIgnoredProperties()) {
                if (p.matcher(r.getURI()).find()) {
                    return true;
                }
            }
        }
        return false;
    }

    private Map<TinyResource, Set<TinyResource>> computeProofSetsForProperties(Subgraph G, Set<TinyResource> subjects) {
        Map<TinyResource, Set<TinyResource>> result = new HashMap<>();
        for (Map.Entry<TinyResource, Map<TinyNode, List<Triple>>> i : G.getPOSIndex().entrySet()) {
            TinyResource p = i.getKey();
            if (isIgnored(p)) {
                continue;
            }
            Set<TinyResource> pSubjects = new HashSet<>();
            for (List<Triple> triples : i.getValue().values()) {
                for (Triple t : triples) {
                    pSubjects.add(t.getS());
                }
            }
            pSubjects.retainAll(subjects);
            result.put(p, pSubjects);
        }
        return result;
    }

    private Set<TinyResource> filterResources(Set<TinyNode> input) {
        HashSet<TinyResource> result = new HashSet<>();
        for (TinyNode i : input) {
            if (i.isURIResource()) {
                result.add(i.asResource());
            }
        }
        return result;
    }

    private List<PartialPattern> computeClosedIntersections(List<PartialPattern> patterns) {
        boolean[] done = new boolean[patterns.size()];
        List<PartialPattern> result = new ArrayList<>();
        for (int i = 0; i < done.length; ++i) {
            done[i] = false;
        }
        for (int i = 0; i < patterns.size(); ++i) {
            if (done[i]) {
                continue;
            }
            Set<PartialPattern> others = new HashSet<>();
            PartialPattern current = patterns.get(i);
            for (int j = i + 1; j < patterns.size(); ++j) {
                if (done[j]) {
                    continue;
                }
                PartialPattern other = patterns.get(j);
                if (Objects.equals(current.getProofSet(), other.getProofSet())) {
                    others.add(other);
                    done[j] = true;
                }
            }
            if (others.size() > 0) {
                others.add(current);
                current = new IntersectionPattern(others, current.getQuality(), current.getProofSet());
            }
            result.add(current);
        }
        return result;
    }

    public List<PartialPattern> magic(final Weight w, SLDMListener monitor) {
        List<PartialPattern> result = new ArrayList<>();
        monitor.stateChanged(SLDMListener.State.SPARQL);
        final Set<TinyResource> uris = filterResources(w.getBase());
        final Subgraph g = new Subgraph(cfg, sparql, uris);
        if (fullGraph != null) {
            fullGraph.addSubgraph(g);
        }
        logger.debug(String.format("Subgraph of %d triples", g.getTriplesCount()));
        if (logger.isTraceEnabled()) {
            Iterator<Triple> i = g.getTriples();
            while (i.hasNext()) {
                Triple t = i.next();
                logger.trace(t);
            }
        }
        monitor.stateChanged(SLDMListener.State.MINING);
        Map<TinyResource, Set<TinyResource>> properties = computeProofSetsForProperties(g, uris);
        List<Callable<List<PartialPattern>>> tasks = new ArrayList<>();
        for (final Map.Entry<TinyResource, Set<TinyResource>> i : properties.entrySet()) {
            Callable<List<PartialPattern>> task = new Callable<List<PartialPattern>>() {

                @Override
                public List<PartialPattern> call() throws Exception {
                    TinyResource p = i.getKey();
                    Set<TinyResource> pSubjects = i.getValue();
                    assert uris.containsAll(pSubjects);
                    if (w.support(pSubjects) < cfg.getMinSupport()) {
                        return Collections.EMPTY_LIST;
                    }
                    List<PartialPattern> patterns = new ArrayList<>();
                    for (Rule r : rules) {
                        patterns.addAll(r.find(g, p, w));
                    }
                    if (patterns.isEmpty()) {
                        //fallback, objects patterns is a very general one
                        Weight wo = new Weight(w, g, p);
                        double s = wo.getSum();
                        if (s >= cfg.getMinSupport()) {
                            if(!PartialPattern.RDF_TYPE.equals(p.getURI()))
                                patterns.add(new ObjectsPattern(p, wo, s, pSubjects));
                        } else {
                            //TODO: p some Thing czy cos takiego
                        }
                    }
                    return patterns;
                }
            };
            tasks.add(task);
        }
        try {
            List<Future<List<PartialPattern>>> futures = executorService.invokeAll(tasks);
            for (Future<List<PartialPattern>> future : futures) {
                assert future.isDone();
                result.addAll(future.get());
            }
        } catch (InterruptedException | ExecutionException ex) {
            throw new RuntimeException(ex);
        }
        return result;
    }

    private boolean run(Weight individuals, int level, SLDMListener monitor, List<PartialPattern> path, PartialPatternProcessor processor, boolean computeIntersections) {
        List<PartialPattern> patterns = magic(individuals, monitor);
        List<ObjectsPattern> queue = new ArrayList<>();
        for (PartialPattern p : patterns) {
            if (p instanceof ObjectsPattern && level > 0) {
                queue.add((ObjectsPattern) p);
            }
        }
        if (computeIntersections) {
            patterns = computeClosedIntersections(patterns);
        }
        for (PartialPattern p : patterns) {
            logger.debug(String.format("%s", p));
            if (!processor.process(path, p)) {
                return false;
            }
        }
        for (ObjectsPattern tp : queue) {
            Weight objects = tp.getObjects();
            path.add(tp);
            if (!run(objects, level - 1, monitor, path, processor, true)) {
                return false;
            }
            path.remove(path.size() - 1);
//            objects.clear();    //to save memory
        }
        return true;
    }

    private List<TinyResource> sample(List<TinyResource> input, int size) {
        return SamplingStrategyFactory.INSTANCE.get(cfg.getSamplingStrategy(), sparql, rnd).sample(input, size);
    }

    public void run(boolean validate, final SLDMListener processor) {
        executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        if (cfg.getRemoteValidation()) {
            fullGraph = null;
        } else {
            fullGraph = new JenaGraph();
        }
        try {
            base = sparql.listResources(cfg.getInitialQuery());
            if (cfg.isSamplingEnabled()) {
                base = sample(base, cfg.getSampleSize());
            }
            logger.debug(String.format("Found %d individuals of base class", base.size()));
            if (!run(new Weight(base), cfg.getMaxLevel(), processor, new ArrayList<PartialPattern>(), new PartialPatternProcessor() {

                @Override
                public boolean process(List<PartialPattern> path, PartialPattern pattern) {
                    if (cfg.getMaxOpenLevel() <= cfg.getMaxLevel() && path.size() + 1 >= cfg.getMaxOpenLevel() && pattern.isOpen()) {
                        return true;
                    }
                    processor.patternMined(new FullPattern(path, pattern));
                    return !processor.shouldStop();
                }
            }, false)) {
                return;
            }
            if (validate) {
                throw new UnsupportedOperationException();
//                validate(result, monitor);
            }
        } finally {
            if (fullGraph != null) {
                fullGraph.dispose();
                fullGraph = null;
            }
            processor.stateChanged(SLDMListener.State.IDLE);
            executorService.shutdown();
            executorService = null;
        }
    }

    public FrequentPatterns run(boolean validate) {
        FrequentPatternsProcessor processor = new FrequentPatternsProcessor();
        run(validate, processor);
        return processor.deliver();
    }

    private void validate(List<FullPattern> result, SLDMListener monitor) {
        monitor.stateChanged(SLDMListener.State.VALIDATING);
        double step = 100.0 / result.size();
        for (FullPattern p : result) {
            String sparqlPatterns = p.toSPARQL("?x", new DefaultVariableGenerator());
            logger.debug("Validating pattern: " + sparqlPatterns);
            int n;
            if (fullGraph == null) {
                n = sparql.count(base, sparqlPatterns);
            } else {
                n = fullGraph.countMatching(sparqlPatterns, "?x", base);
            }
            p.setQuality(n);
            logger.debug(n);
        }
        Collections.sort(result, new Comparator<FullPattern>() {

            @Override
            public int compare(FullPattern o1, FullPattern o2) {
                return o2.getQuality() - o1.getQuality();
            }
        });
    }

    public List<TinyResource> getBase() {
        return Collections.unmodifiableList(base);
    }

    public SLDM(Config cfg, SPARQL sparql) {
        this.cfg = cfg;
        this.sparql = sparql;
        this.rules = new ArrayList<>();
        if (cfg.getRandomSeed() != null) {
            this.rnd = new Random(cfg.getRandomSeed());
        } else {
            this.rnd = new Random();
        }
        rules.add(new SelfRule(cfg));
        rules.add(new FillerRule(cfg));
        rules.add(new DatatypeRule(cfg));
        rules.add(new NumericRule(cfg));
    }
}
