package put.sldm;

import com.hp.hpl.jena.sparql.util.FmtUtils;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLLiteral;
import put.sldm.tiny.TinyLiteral;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class Utils {

    public static String toString(TinyNode n) {
        if (n.isResource()) {
            return toString(n.asResource());
        } else {
            return n.asLiteral().toString();
        }
    }

    public static String toString(TinyResource r) {
        return FmtUtils.stringForURI(r.getURI());
    }

    public static OWLLiteral convert(OWLDataFactory m, TinyLiteral l) {
        OWLDatatype dt = null;
        if (l.getDatatype() != null) {
            dt = m.getOWLDatatype(IRI.create(l.getDatatypeURI()));
        }
        if (l.getLanguage().isEmpty()) {
            return m.getOWLLiteral(l.getLexicalForm(), dt);
        } else {
            return m.getOWLLiteral(l.getLexicalForm(), l.getLanguage());
        }
    }

}
