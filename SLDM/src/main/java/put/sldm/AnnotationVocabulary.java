package put.sldm;

import org.semanticweb.owlapi.model.IRI;

public class AnnotationVocabulary {

    public static IRI QUALITY = IRI.create("http://semantic.cs.put.poznan.pl/sldm/ontology#quality");
    public static IRI SPARQL_REPRESENTATION = IRI.create("http://semantic.cs.put.poznan.pl/sldm/ontology#sparqlRepresentation");
}
