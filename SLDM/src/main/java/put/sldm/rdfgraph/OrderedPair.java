package put.sldm.rdfgraph;

import java.util.Objects;

public class OrderedPair<A, B> {

    public final A first;
    public final B second;

    public OrderedPair(A first, B second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof OrderedPair)) {
            return false;
        }
        OrderedPair other = (OrderedPair) obj;
        return Objects.equals(this.first, other.first) && Objects.equals(this.second, other.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

}
