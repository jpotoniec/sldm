package put.sldm.rdfgraph;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import java.io.FileOutputStream;
import java.util.Iterator;
import java.util.List;
import org.apache.log4j.Logger;
import put.sldm.Utils;
import put.sldm.tiny.TinyResource;

public class JenaGraph implements Graph {

    private final static Logger logger = Logger.getLogger(JenaGraph.class);

    private final Model model = ModelFactory.createDefaultModel();

    @Override
    public void addSubgraph(Subgraph g) {
        Iterator<Triple> i = g.getTriples();
        while (i.hasNext()) {
            Triple t = i.next();
            Resource s = model.createResource(t.getS().getURI());
            Property p = model.createProperty(t.getP().getURI());
            RDFNode o;
            if (t.getO().isResource()) {
                o = model.createResource(t.getO().asResource().getURI());
            } else {
                o = t.getO().asLiteral().toJena(model);
            }
            model.add(s, p, o);
        }
        logger.debug(String.format("Graph has %d statements", model.size()));
    }

    @Override
    public int countMatching(String pattern, String rootVariable, List<TinyResource> subjects) {
        QueryExecution qe = null;
        try {
            String values = "";
            for (TinyResource s : subjects) {
                values += Utils.toString(s) + " ";
            }
            String query = String.format("select (count(distinct %1$s) as ?count) where {%2$s\n values %1$s {%3$s}}", rootVariable, pattern, values);
            qe = QueryExecutionFactory.create(query, model);
            ResultSet rs = qe.execSelect();
            if (rs.hasNext()) {
                QuerySolution qs = rs.next();
                return qs.getLiteral(qs.varNames().next()).getInt();
            }
            qe.close();
        } catch (Exception ex) {
            logger.error("counting", ex);
        } finally {
            if (qe != null) {
                qe.close();
            }
        }
        return 0;
    }

    @Override
    public void dispose() {
        try (FileOutputStream fs = new FileOutputStream("/tmp/full_graph.ttl")) {
            model.write(fs, "TTL");
        } catch (Exception ex) {
            logger.error("Writing full graph", ex);
        }
        model.close();
    }

}
