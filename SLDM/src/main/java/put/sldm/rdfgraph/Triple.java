package put.sldm.rdfgraph;

import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class Triple {

    private final TinyResource s;
    private final TinyResource p;
    private final TinyNode o;

    public Triple(Resource s, Resource p, RDFNode o) {
        this.s = new TinyResource(s);
        this.p = new TinyResource(p);
        this.o = TinyNode.produce(o);
    }

    public Triple(TinyResource s, TinyResource p, TinyNode o) {
        this.s = s;
        this.p = p;
        this.o = o;
    }

    public TinyResource getS() {
        return s;
    }

    public TinyResource getP() {
        return p;
    }

    public TinyNode getO() {
        return o;
    }

    @Override
    public String toString() {
        return String.format("(%s, %s, %s)", s, p, o);
    }

}
