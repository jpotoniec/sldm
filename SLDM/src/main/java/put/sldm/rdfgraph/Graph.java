package put.sldm.rdfgraph;

import java.util.List;
import put.sldm.tiny.TinyResource;

public interface Graph {

    public void addSubgraph(Subgraph g);

    public int countMatching(String pattern, String rootVariable, List<TinyResource> subjects);
    
    public void dispose();
}
