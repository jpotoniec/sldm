package put.sldm.rdfgraph;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.RDFNode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import put.sldm.SPARQL;
import put.sldm.config.Config;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class Subgraph {

    private int triplesCount;
    private final Config cfg;
    private final SPARQL sparql;
    private final DoubleIndex index;

    private void extract(Collection<TinyResource> resources) {
        sparql.iteratedQuery("?s ?p ?o", "?s ?p ?o", "?s", new ArrayList<>(resources), new SPARQL.SolutionProcessor() {

            @Override
            public void process(QuerySolution qs) {
                RDFNode s = qs.get("?s");
                RDFNode p = qs.get("?p");
                RDFNode o = qs.get("?o");
                if (p.isURIResource()) {
                    Triple t = new Triple(s.asResource(), p.asResource(), o);
                    triplesCount++;
                    index.add(t);
                }
            }
        });

        index.trimToSize();
    }

    public Subgraph(Config cfg, SPARQL sparql, Set<TinyResource> resources) {
        this.cfg = cfg;
        this.sparql = sparql;
        this.index = new DoubleIndex();
        extract(resources);
    }

    public int getTriplesCount() {
        return triplesCount;
    }

    public Iterator<Triple> getTriples() {
        return index.getTriplesIterator();
    }

    public Map<TinyResource, Map<TinyNode, List<Triple>>> getPOSIndex() {
        return index.getData();
    }

    public Map<TinyNode, List<Triple>> getOSforP(TinyResource p) {
        return index.getData().get(p);
    }

}
