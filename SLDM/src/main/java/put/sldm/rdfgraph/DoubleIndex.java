package put.sldm.rdfgraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

class IndexIterator<K, V> implements Iterator<V> {

    private final Iterator<List<V>> i;
    private Iterator<V> j;

    public IndexIterator(Map<K, List<V>> data) {
        i = data.values().iterator();
        j = null;
    }

    @Override
    public boolean hasNext() {
        if (j != null) {
            if (j.hasNext()) {
                return true;
            } else {
                j = null;
            }
        }
        return i.hasNext();
    }

    @Override
    public V next() {
        if (j == null) {
            j = i.next().iterator();
        }
        return j.next();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class DoubleIterator implements Iterator<Triple> {

    private final Iterator<Map<TinyNode, List<Triple>>> pIterator;
    private Iterator<List<Triple>> oIterator;
    private Iterator<Triple> sIterator;

    public DoubleIterator(Map<TinyResource, Map<TinyNode, List<Triple>>> data) {
        pIterator = data.values().iterator();
    }

    @Override
    public boolean hasNext() {
        if (sIterator != null) {
            if (sIterator.hasNext()) {
                return true;
            } else {
                sIterator = null;
            }
        }
        if (oIterator != null) {
            if (oIterator.hasNext()) {
                return true;
            } else {
                oIterator = null;
            }
        }
        return pIterator.hasNext();
    }

    @Override
    public Triple next() {
        if (sIterator == null) {
            if (oIterator == null) {
                oIterator = pIterator.next().values().iterator();
            }
            sIterator = oIterator.next().iterator();
        }
        return sIterator.next();
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

class SpIterator implements Iterator<OrderedPair<TinyResource, TinyResource>> {

    private final Iterator<Map.Entry<TinyResource, Map<TinyResource, List<Triple>>>> pIterator;
    private Iterator<TinyResource> sIterator;
    private TinyResource p;

    public SpIterator(Map<TinyResource, Map<TinyResource, List<Triple>>> data) {
        this.pIterator = data.entrySet().iterator();
    }

    @Override
    public boolean hasNext() {
        if (sIterator != null) {
            if (sIterator.hasNext()) {
                return true;
            } else {
                sIterator = null;
            }
        }
        return pIterator.hasNext();
    }

    @Override
    public OrderedPair<TinyResource, TinyResource> next() {
        if (sIterator == null) {
            Map.Entry<TinyResource, Map<TinyResource, List<Triple>>> x = pIterator.next();
            p = x.getKey();
            sIterator = x.getValue().keySet().iterator();
        }
        TinyResource s = sIterator.next();
        return new OrderedPair<>(s, p);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

public class DoubleIndex {

    //p->o->triples
    private final Map<TinyResource, Map<TinyNode, List<Triple>>> data;

    public DoubleIndex() {
        this.data = new HashMap<>();
    }

    public void add(Triple t) {
        Map<TinyNode, List<Triple>> oIndex = data.get(t.getP());
        if (oIndex == null) {
            oIndex = new HashMap<>();
            data.put(t.getP(), oIndex);
        }
        List<Triple> triples = oIndex.get(t.getO());
        if (triples == null) {
            triples = new ArrayList<>();
            oIndex.put(t.getO(), triples);
        }
        triples.add(t);
    }

    public Iterator<Triple> getTriplesIterator() {
        return new DoubleIterator(data);
    }

    public void trimToSize() {
        for (Map<TinyNode, List<Triple>> m : data.values()) {
            for (List<Triple> l : m.values()) {
                ((ArrayList) l).trimToSize();
            }
        }
    }
    
    public Map<TinyResource, Map<TinyNode, List<Triple>>> getData() {
        return data;
    }
//
//    public Iterator<Triple> find(TinyResource s, TinyResource p) {
//        if (p == null) {
//            throw new NullPointerException();
//        }
//        Map<TinyResource, List<Triple>> sIndex = data.get(p);
//        if (sIndex == null) {
//            return Collections.emptyIterator();
//        }
//        if (s == null) {
//            return new IndexIterator<>(sIndex);
//        }
//        List<Triple> l = sIndex.get(s);
//        if (l == null) {
//            return Collections.emptyIterator();
//        }
//        return l.iterator();
//    }
//
//    public int count(TinyResource s, TinyResource p) {
//        if (p == null) {
//            throw new NullPointerException();
//        }
//        Map<TinyResource, List<Triple>> sIndex = data.get(p);
//        if (sIndex == null) {
//            return 0;
//        }
//        if (s == null) {
//            int result = 0;
//            for (List<Triple> l : sIndex.values()) {
//                result += l.size();
//            }
//            return result;
//        }
//        List<Triple> l = sIndex.get(s);
//        if (l == null) {
//            return 0;
//        }
//        return l.size();
//    }
}
