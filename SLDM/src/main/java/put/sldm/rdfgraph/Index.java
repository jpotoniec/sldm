package put.sldm.rdfgraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Index<K, V> extends HashMap<K, List<V>> implements Iterable<V> {

    private class IndexIterator implements Iterator<V> {

        private final Iterator<Map.Entry<K, List<V>>> i;
        private Iterator<V> j;

        public IndexIterator() {
            i = entrySet().iterator();
            j = null;
        }

        @Override
        public boolean hasNext() {
            if (j != null) {
                if (j.hasNext()) {
                    return true;
                } else {
                    j = null;
                }
            }
            return i.hasNext();
        }

        @Override
        public V next() {
            if (j == null) {
                j = i.next().getValue().iterator();
            }
            return j.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    public void trimToSize() {
        for (List<V> l : values()) {
            ((ArrayList) l).trimToSize();
        }
    }

    public void add(K key, V value) {
        List<V> values = super.get(key);
        if (values == null) {
            values = new ArrayList<>();
            put(key, values);
        }
        values.add(value);
    }

    @Override
    public List<V> get(Object key) {
        List<V> result = super.get(key);
        if (result != null) {
            return result;
        } else {
            return Collections.EMPTY_LIST;
        }
    }

    @Override
    public Iterator<V> iterator() {
        return new IndexIterator();
    }

}
