package put.sldm;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.sparql.engine.http.QueryEngineHTTP;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import put.sldm.config.Config;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;

public class SPARQL {

    private final static Logger logger = Logger.getLogger(SPARQL.class);
    private final Config cfg;
    private static int queryCounter = 0;

    public SPARQL(Config cfg) {
        this.cfg = cfg;
    }

    public static int getQueryCounter() {
        return queryCounter;
    }

    protected QueryExecution produceQueryExecution(Config cfg, String query) {
        queryCounter++;
        return new QueryEngineHTTP(cfg.getEndpoint(), query);
    }

    public interface SolutionProcessor {

        public void process(QuerySolution partialSolution);
    }

    public class SummingProcessor implements SolutionProcessor {

        private int result = 0;

        public int getResult() {
            return result;
        }

        @Override
        public void process(QuerySolution s) {
            result += s.getLiteral(s.varNames().next()).getInt();
        }

    }

    public void iteratedQuery(String head, String pattern, String var, List<? extends TinyNode> objects, SolutionProcessor processor) {
        this.iteratedQuery(head, pattern, var, "", null, objects, processor);
    }

    public void iteratedQuery(String head, String pattern, String var, String suffix, Integer step, List<? extends TinyNode> objects, SolutionProcessor processor) {
        if(step == null)
            step = cfg.getMaxValuesSize();
        for (int i = 0; i < objects.size(); i += step) {
            String values = "";
            for (TinyNode s : objects.subList(i, Math.min(i + step, objects.size()))) {
                String uri = Utils.toString(s);
                if (uri.contains("`") || uri.contains("'") || uri.contains("\"") || uri.contains(" "))   //work-around for BLZG-8239 https://jira.blazegraph.com/browse/BLZG-8239
                    continue;
                values += uri + " ";
            }
            String query = String.format("select %s where {\n%s\nvalues %s {%s}\n}%s", head, pattern, var, values, suffix);
            query(query, processor);
        }
    }

    protected void query(String query, SolutionProcessor processor) {
        try (QueryExecution qe = produceQueryExecution(cfg, query)) {
            ResultSet results = qe.execSelect();
            while (results.hasNext()) {
                try {
                    processor.process(results.next());
                } catch (Exception ex) {
                    logger.warn(query, ex);
                }
            }
        } catch (Exception ex) {
            logger.error(query, ex);
            throw ex;
        }
    }

    public int count(List<TinyResource> objects, String pattern) {
        SummingProcessor proc = new SummingProcessor();
        iteratedQuery("(count(distinct ?x) as ?count_value)", pattern, "?x", objects, proc);
        return proc.getResult();
    }

    public ArrayList<TinyResource> listResources(String q) {
        final ArrayList<TinyResource> individuals = new ArrayList<>();
        query(q, new SolutionProcessor() {

            @Override
            public void process(QuerySolution qs) {
                individuals.add(new TinyResource(qs.getResource(qs.varNames().next())));
            }
        });
        return individuals;
    }
}
