package put.sldm.sampling;

import put.sldm.tiny.TinyResource;

import java.util.Collections;
import java.util.List;
import java.util.Random;

public class UniformStrategy implements SamplingStrategy {

    private final Random rnd;

    public UniformStrategy(Random rnd) {
        this.rnd = rnd;
    }

    @Override
    public List<TinyResource> sample(List<TinyResource> input, int size) {
        if (input.size() > size) {
            Collections.shuffle(input, rnd);
            return input.subList(0, size);
        } else {
            return input;
        }
    }
}
