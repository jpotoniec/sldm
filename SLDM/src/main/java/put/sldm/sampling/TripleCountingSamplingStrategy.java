package put.sldm.sampling;

import put.sldm.SPARQL;

import java.util.Random;

public class TripleCountingSamplingStrategy extends AbstractCountingSamplingStrategy {


    public TripleCountingSamplingStrategy(SPARQL sparql, Random rnd) {
        super(sparql, rnd, "?s (count(?p) as ?c)", "?s ?p ?o", "?s", "group by ?s", false, 1);
    }
}
