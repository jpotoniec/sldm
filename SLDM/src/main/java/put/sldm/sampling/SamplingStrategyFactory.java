package put.sldm.sampling;

import put.sldm.SPARQL;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SamplingStrategyFactory {

    public enum SamplingStrategyId {
        UNIFORM, PREDICATES_COUNTING, TRIPLES_COUNTING
    }

    private SamplingStrategyFactory() {

    }

    public static SamplingStrategyFactory INSTANCE = new SamplingStrategyFactory();

    public SamplingStrategy get(SamplingStrategyId id, SPARQL sparql, Random rnd) {
        switch (id) {
            case UNIFORM:
                return new UniformStrategy(rnd);
            case PREDICATES_COUNTING:
                return new PredicateCountingSamplingStrategy(sparql, rnd);
            case TRIPLES_COUNTING:
                return new TripleCountingSamplingStrategy(sparql, rnd);
        }
        throw new IllegalArgumentException("Unknown sampling strategy id");
    }
}
