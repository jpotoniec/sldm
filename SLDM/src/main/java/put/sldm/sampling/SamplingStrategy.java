package put.sldm.sampling;

import put.sldm.tiny.TinyResource;

import java.util.List;

public interface SamplingStrategy {
    List<TinyResource> sample(List<TinyResource> input, int size);
}
