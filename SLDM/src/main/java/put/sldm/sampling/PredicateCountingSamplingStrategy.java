package put.sldm.sampling;

import put.sldm.SPARQL;

import java.util.Random;

public class PredicateCountingSamplingStrategy extends AbstractCountingSamplingStrategy {


    public PredicateCountingSamplingStrategy(SPARQL sparql, Random rnd) {
        super(sparql, rnd, "?s (count(distinct ?p) as ?c)", "?s ?p []", "?s", "group by ?s", false, null);
    }
}
