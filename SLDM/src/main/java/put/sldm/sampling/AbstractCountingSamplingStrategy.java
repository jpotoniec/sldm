package put.sldm.sampling;

import com.hp.hpl.jena.query.QuerySolution;
import put.sldm.SPARQL;
import put.sldm.tiny.TinyResource;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class AbstractCountingSamplingStrategy implements SamplingStrategy {

    private final Random rnd;
    private final String head;
    private final String pattern;
    private final String var;
    private final String suffix;
    private final boolean withReplacement;
    private SPARQL sparql;
    private Integer step;


    public AbstractCountingSamplingStrategy(SPARQL sparql, Random rnd, String head, String pattern, String var, String suffix, boolean withReplacement, Integer step) {
        this.sparql = sparql;
        this.rnd = rnd;
        this.head = head;
        this.pattern = pattern;
        this.var = var;
        this.suffix = suffix;
        this.withReplacement = withReplacement;
        this.step = step;
    }

    @Override
    public List<TinyResource> sample(final List<TinyResource> input, int size) {
        if (withReplacement || input.size() > size) {
            final int[] values = new int[input.size()];
            sparql.iteratedQuery(head, pattern, var, suffix, step, input, new SPARQL.SolutionProcessor() {

                @Override
                public void process(QuerySolution partialSolution) {
                    TinyResource r = new TinyResource(partialSolution.get("s").asResource());
                    values[input.indexOf(r)] = partialSolution.get("c").asLiteral().getInt();
                }
            });
            int sum = 0;
            for (int i = 0; i < input.size(); i++) {
                sum += values[i];
            }
            List<TinyResource> result = new ArrayList<>();
            for (int n = 0; n < size; n++) {
                int val = rnd.nextInt(sum);
                int pos = 0;
                while (values[pos] < val) {
                    pos += 1;
                    val -= values[pos];
                }
                if(!withReplacement) {
                    sum -= values[pos];
                    values[pos] = 0;
                }
                result.add(input.get(pos));
            }
            return result;
        } else {
            return input;
        }
    }
}
