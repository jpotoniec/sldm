package put.sldm;

import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import put.sldm.patterns.FullPattern;

public class FrequentPatterns {

    private final List<FullPattern> patterns;
    private final int maxQuality;
    private final GregorianCalendar begin, end;

    public FrequentPatterns(List<FullPattern> patterns, int maxQuality, GregorianCalendar begin, GregorianCalendar end) {
        this.patterns = patterns;
        this.maxQuality = maxQuality;
        this.begin = begin;
        this.end = end;
    }

    public List<FullPattern> getPatterns() {
        return Collections.unmodifiableList(patterns);
    }

    public int getMaxQuality() {
        return maxQuality;
    }

    public GregorianCalendar getBegin() {
        return begin;
    }

    public GregorianCalendar getEnd() {
        return end;
    }
}
