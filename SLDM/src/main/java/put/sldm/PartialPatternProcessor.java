package put.sldm;

import java.util.List;
import put.sldm.patterns.partial.PartialPattern;

public interface PartialPatternProcessor {

    /**
     * Method called for every mined pattern
     * @param path
     * @param pattern
     * @return true if mining is to be continued, false to stop the mining
     */
    public boolean process(List<PartialPattern> path, PartialPattern pattern);
}
