package put.sldm;

import put.sldm.patterns.FullPattern;

public interface SLDMListener {

    public void miningStarted();

    public void baseDetermined(int size);

    public void miningFinished();

    public void patternMined(FullPattern p);

    public boolean shouldStop();

    public enum State {

        IDLE, SPARQL, MINING, VALIDATING
    };

    public void stateChanged(State current);
}
