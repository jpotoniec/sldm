/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package put.sldm.rdfgraph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import put.sldm.tiny.TinyResource;
import static org.hamcrest.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class DoubleIndexTest {

    @Mock
    private TinyResource s1, s2, s3, s4, s5;
    @Mock
    private TinyResource p1, p2, p3, p4, p5;
    @Mock
    private TinyResource o1, o2, o3, o4, o5, o6, o7, o8;
    private Triple[] t;

    private DoubleIndex i;

    @Before
    public void setup() {
        i = new DoubleIndex();
        t = new Triple[]{
            new Triple(s1, p1, o1),
            new Triple(s1, p1, o2),
            new Triple(s1, p2, o3),
            new Triple(s1, p2, o4),
            new Triple(s2, p1, o5),
            new Triple(s2, p1, o6),
            new Triple(s2, p2, o7),
            new Triple(s2, p2, o8)
        };
        for (Triple x : t) {
            i.add(x);
        }
    }

    private <T> List<T> unwind(Iterator<T> it, List<T> l) {
        while (it.hasNext()) {
            l.add(it.next());
        }
        return l;
    }

    @Test
    public void triplesIterator() {
        assertThat(unwind(i.getTriplesIterator(), new ArrayList<Triple>()), containsInAnyOrder(t));
    }
}
