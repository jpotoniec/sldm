package put.sldm;


import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.Model;
import put.sldm.config.Config;

public class LocalSPARQL extends SPARQL {

    private final Model model;

    public LocalSPARQL(Config cfg, Model model) {
        super(cfg);
        this.model = model;
    }

    @Override
    protected QueryExecution produceQueryExecution(Config cfg, String query) {
        return QueryExecutionFactory.create(query, model);
    }

}