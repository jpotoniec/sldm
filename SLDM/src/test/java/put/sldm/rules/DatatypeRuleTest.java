
package put.sldm.rules;

import org.junit.Test;
import static org.junit.Assert.*;

public class DatatypeRuleTest {
    
    @Test
    public void dateTimeTest() {
        assertTrue(DatatypeRule.DATETIME_PATTERN.matcher("2002-10-10T12:00:00-05:00").matches());
        assertTrue(DatatypeRule.DATETIME_PATTERN.matcher("2002-10-10T17:00:00Z").matches());
    }
}
