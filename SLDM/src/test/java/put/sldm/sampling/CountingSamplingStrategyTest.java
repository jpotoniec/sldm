package put.sldm.sampling;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import org.junit.Before;
import org.junit.Test;
import put.sldm.LocalSPARQL;
import put.sldm.SPARQL;
import put.sldm.config.Config;
import put.sldm.tiny.TinyResource;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

public class CountingSamplingStrategyTest {

    private SPARQL sparql;
    private Config cfg;

    @Before
    public void before() throws FileNotFoundException {
        Model m = ModelFactory.createDefaultModel();
        m.read(new FileInputStream("src/test/resources/test1.ttl"), "http://example.com/foo#", "TTL");
        cfg = new Config() {

            @Override
            public String getEndpoint() {
                return "";
            }

            @Override
            public String getInitialQuery() {
                return "select distinct ?s where {?s a <http://example.com/foo#C>}";
            }

            @Override
            public double getMinFunctionalSupport() {
                return .25;
            }

            @Override
            public List<Pattern> getIgnoredProperties() {
                return Collections.EMPTY_LIST;
            }

            @Override
            public int getMaxLevel() {
                return 2;
            }

            @Override
            public int getMaxOpenLevel() {
                return 1;
            }

            @Override
            public int getMaxValuesSize() {
                return 100;
            }

            @Override
            public double getMinSupport() {
                return .9;
            }

            @Override
            public Long getRandomSeed() {
                return null;
            }

            @Override
            public boolean getRemoteValidation() {
                return false;
            }

            @Override
            public int getSampleSize() {
                return 0;
            }

            @Override
            public boolean isSamplingEnabled() {
                return false;
            }

            @Override
            public SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy() {
                return SamplingStrategyFactory.SamplingStrategyId.UNIFORM;
            }

        };
        sparql = new LocalSPARQL(cfg, m);
    }

    @Test
    public void testTripleCounting() throws FileNotFoundException {
        TripleCountingSamplingStrategy s = new TripleCountingSamplingStrategy(sparql, new Random(42));
        ArrayList<TinyResource> resources = sparql.listResources(cfg.getInitialQuery());
        List<TinyResource> result = s.sample(resources, 5);
        assertEquals(5, result.size());
    }

    @Test
    public void testPredicateCounting() throws FileNotFoundException {
        PredicateCountingSamplingStrategy s = new PredicateCountingSamplingStrategy(sparql, new Random(42));
        ArrayList<TinyResource> resources = sparql.listResources(cfg.getInitialQuery());
        List<TinyResource> result = s.sample(resources, 5);
        assertEquals(5, result.size());
    }
}