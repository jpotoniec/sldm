/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package put.sldm;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.vocabulary.RDF;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import put.sldm.config.Config;
import put.sldm.patterns.FullPattern;
import put.sldm.patterns.partial.ConcretePattern;
import put.sldm.patterns.partial.DatatypePropertyPattern;
import put.sldm.patterns.partial.FunctionalPattern;
import put.sldm.patterns.partial.PartialPattern;
import put.sldm.patterns.partial.RangePattern;
import put.sldm.patterns.partial.SelfPattern;
import put.sldm.sampling.SamplingStrategy;
import put.sldm.sampling.SamplingStrategyFactory;
import put.sldm.tiny.TinyLiteral;
import put.sldm.tiny.TinyNode;
import put.sldm.tiny.TinyResource;


public class SLDMTest {

    public static void check(List<FullPattern> patterns, PartialPattern... expected) {
        FullPattern e = new FullPattern(Arrays.asList(expected));
        assertTrue(patterns.contains(e));
    }

    @Test
    public void test1() throws FileNotFoundException {
        Model m = ModelFactory.createDefaultModel();
        m.read(new FileInputStream("src/test/resources/test1.ttl"), "http://example.com/foo#", "TTL");
        TinyResource C = new TinyResource(m.createResource("http://example.com/foo#C"));
        TinyResource hasTemperature = new TinyResource(m.createResource("http://example.com/foo#hasTemperature"));
        TinyResource origin = new TinyResource(m.createResource("http://example.com/foo#origin"));
        TinyResource year = new TinyResource(m.createResource("http://example.com/foo#year"));
        TinyResource someDate = new TinyResource(m.createResource("http://example.com/foo#someDate"));
        TinyResource hasName = new TinyResource(m.createResource("http://example.com/foo#hasName"));
        TinyResource self = new TinyResource(m.createResource("http://example.com/foo#self"));
        TinyLiteral en = new TinyLiteral(m.createTypedLiteral("en"));
        TinyLiteral pl = new TinyLiteral(m.createTypedLiteral("pl"));
        Config cfg = new Config() {

            @Override
            public String getEndpoint() {
                return "";
            }

            @Override
            public String getInitialQuery() {
                return "select distinct ?s where {?s a <http://example.com/foo#C>}";
            }

            @Override
            public double getMinFunctionalSupport() {
                return .25;
            }

            @Override
            public List<Pattern> getIgnoredProperties() {
                return Collections.EMPTY_LIST;
            }

            @Override
            public int getMaxLevel() {
                return 2;
            }

            @Override
            public int getMaxOpenLevel() {
                return 1;
            }

            @Override
            public int getMaxValuesSize() {
                return 100;
            }

            @Override
            public double getMinSupport() {
                return .9;
            }

            @Override
            public Long getRandomSeed() {
                return null;
            }

            @Override
            public boolean getRemoteValidation() {
                return false;
            }

            @Override
            public int getSampleSize() {
                return 0;
            }

            @Override
            public boolean isSamplingEnabled() {
                return false;
            }

            @Override
            public SamplingStrategyFactory.SamplingStrategyId getSamplingStrategy() {
                return SamplingStrategyFactory.SamplingStrategyId.UNIFORM;
            }

        };
        SPARQL sparql = new LocalSPARQL(cfg, m);
        List<FullPattern> patterns = new SLDM(cfg, sparql).run(false).getPatterns();
        System.err.printf("SLDM generated %d patterns\n", patterns.size());
        for (FullPattern p : patterns) {
            System.err.println(p.toString());
        }
//        assertEquals(21, patterns.size());
        check(patterns, new ConcretePattern(new TinyResource(RDF.type), C, 0, null));
        check(patterns, new SelfPattern(self, 0, null));
//        check(patterns, new RangePattern(hasTemperature, RangePattern.Type.MAX, new TinyLiteral(m.createTypedLiteral((long) 9)), 0));
//        check(patterns, new RangePattern(hasTemperature, RangePattern.Type.MIN, new TinyLiteral(m.createTypedLiteral((long) 0)), 0));
//        check(patterns, new FunctionalPattern(origin, Arrays.asList((TinyNode) en, pl), 0));
//        check(patterns, new DatatypePropertyPattern(year, XSDDatatype.XSDgYear, 0));
//        check(patterns, new DatatypePropertyPattern(someDate, XSDDatatype.XSDdate, 0));
//        check(patterns, new DatatypePropertyPattern(hasTemperature, XSDDatatype.XSDlong, 0));
//        check(patterns, new DatatypePropertyPattern(origin, XSDDatatype.XSDstring, 0));
//        check(patterns, new DatatypePropertyPattern(hasName, XSDDatatype.XSDstring, 0));        
    }
}
